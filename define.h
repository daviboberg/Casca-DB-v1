
#define MAX_CMD_PARAMS 10

//Numero de threads executando tarefas simultaneamente
//OBS: é um valor padrao, nao sera usado haja algum argumento passado pelo parametro -t
#define MAX_THREADS_DEFAULT 10


#define MAX_SQLTHREADS_DEFAULT 5


//Numero de tarefas que seram executadas(esse define eh temporario)
/**<@brief Number of worker tasks. */
#define NUMBER_TASKS 100

#define DEFAULT_PORT_SQL 3331


#define DB_KEY "0131Byd7N220T32qp088kIT53ryT113i"

#define DB_URL "http://storage.securecloud.works:5002/datastores/consumption_test/"


/**<@brief Max number of IO sockets. */
#define MAX_IO_SOCKETS 1000

#ifdef TLS
/**<@brief Cert location. */
#define CERT_SERV_FILE "./cert_serv.pem"

/**<@brief Key Location. */
#define KEY_SERV_FILE  "./key_serv.pem"

/**<@brief Cert location. */
#define CERT_IO_FILE "./cert_io.pem"

/**<@brief Key Location. */
#define KEY_IO_FILE  "./key_io.pem"
#endif

/**<@brief Max number of SERVER sockets. */
#define MAX_SV_SOCKETS 1000


//Timeout do listen (não funciona em infinito devido a necessidade de atualizar a lista)
/**<@brief Time before SERVER select thread refresh SERVER vector. */
#define SV_TIMEOUT_SECONDS 1 


//Timeout dos sockets de IO (não funciona em infinito devido a necessidade de atualizar a lista)
/**<@brief Time before IO select thread refresh IO vector. */
#define IO_TIMEOUT_SECONDS 1 

/*Tamanho do buffer de saída dos sockets de IO*/
/**<@brief Size of socket write buffer. */
#define SOCKET_BUFFER_SIZE 1024

/* */
/**<@brief Max size of the return of a read task. */
#define MAX_READ_BYTES 4096


