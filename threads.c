#include "threads.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h"

int taskID =0;
bool running = 1;
struct timeval b;
int countSec = 0;
struct timeval a;
/*
	Função que as threads de execução vão ficar executando.
*/    		
void stop_all()
{
	running = 0;
}

void* do_task(void * arg)
{
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	int num_tasks;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_task()\n",pthread_self());
    #endif
	do_task_args_t * parameters = (do_task_args_t*) arg;
	pthread_mutex_t * mutex_tasks = parameters->mutex_tasks;
	
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	
	pthread_mutex_lock(mutex_tasks);
	
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	
	info_cmd_queue_t ** tasks = parameters->tasks;
	sem_t * semaphore = parameters->semaphore;
	pthread_mutex_unlock(mutex_tasks);
	info_socket_t * vec = parameters->vec;
	int  vec_lenght = parameters->vec_lenght;
	info_cmd_t * current_task = NULL;
	info_cmd_queue_t * queue_pointer = NULL;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_task: Parametros recebidos\n",pthread_self());
    #endif
	/*Loop de busca e execução de comandos*/
	do
	{
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_task: Aguardando task\n",pthread_self());
        #endif
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		sem_wait(semaphore);
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		if(running == 0)
		{

			break;
		}
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_task: Task detectada na fila\n",pthread_self());
        #endif
		get_task(tasks, &current_task, mutex_tasks,&queue_pointer);

		if(running == 0)
		{

			break;
		}
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_task: Task recebida\n",pthread_self());
        #endif
		if(current_task != NULL)
		{
			#ifdef DEBUG
			syslog(LOG_DEBUG, "[%lu]do_task: Task não nula, executando comando\n",pthread_self());
            #endif
			current_task->cmd(current_task);
			#ifdef DEBUG
			syslog(LOG_DEBUG, "[%lu]do_task: Task concluida\n",pthread_self());
            #endif

			free (queue_pointer);

			
		}
		sem_getvalue(semaphore,  &num_tasks);

		syslog(LOG_DEBUG, "[%lu]do_task: Still running, remaining tasks = %d\n",pthread_self(),num_tasks);

	}while(running == 1 );
	pthread_mutex_unlock(mutex_tasks );
	syslog(LOG_DEBUG, "[%lu]do_task: Dead\n",pthread_self());

	return NULL;

}
/*
Função que pega o primeiro elemento da fila de tarefas e carrega em uma variavel
O parametro tasks é a fila de tarefas
O parametro current_task é o endereço onde será carregado a tarefa removida da fila
O parametro é um ponteiro para o mutex da fila de tarefas
*/
info_cmd_queue_t *
get_task(info_cmd_queue_t ** tasks, info_cmd_t ** current_task, pthread_mutex_t * mutex_tasks,info_cmd_queue_t ** queue_pointer)
{
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_task()\n",pthread_self());
	syslog(LOG_DEBUG, "[%lu]get_task: Aguardando task\n",pthread_self());
    #endif
    //pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_mutex_lock(mutex_tasks);
	//pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	if(running == 0)
	{	
		current_task = NULL;

		return;

	}
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_task: Task detectada na fila\n",pthread_self());
    #endif
	if(*tasks != NULL)
	{
		info_cmd_queue_t * aux = (info_cmd_queue_t *) queue_remove( (queue_t **)tasks, (queue_t *)*tasks);
		
		*current_task = &(aux->cmd);
		*queue_pointer = aux;
		
		

		
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_task: Task retirada da fila\n",pthread_self());
        #endif
	} 
	else
	{
		* current_task = NULL;
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_task: Erro, task nula\n",pthread_self());
        #endif
	}
	pthread_mutex_unlock(mutex_tasks);
	
	syslog(LOG_DEBUG, "[%lu]get_task: task retirada da fila com sucesso\n",pthread_self());
	return queue_pointer;

}
/*
Função que adiciona uma nova task para fila de tarefas
O parametro task_queue é a fila de tarefas que sera adicionada a tarefa
O parametro task é a tarfa a ser adicionada
O parametro semaphore é o semafaro de acesso a fila
*/
int add_task(info_cmd_queue_t ** task_queue, info_cmd_queue_t * task, sem_t * semaphore, pthread_mutex_t * tasks_mutex)
{
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_task()\n",pthread_self());
    #endif
	if(task == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_task: Task nula, abortando operação\n",pthread_self());
        #endif
		return -1;
	}
	if(semaphore == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_task: Semaforo nulo, abortando operação\n",pthread_self());
        #endif
		return -1;
	}
	if(tasks_mutex == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_task: Mutex nulo, abortando operação\n",pthread_self());
        #endif
		return -1;
	}
	pthread_mutex_lock(tasks_mutex);
	int err = queue_append ((queue_t **) task_queue, (queue_t*) task);
	if(err<0)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_task: Erro na inserção na fila, abortando operação\n",pthread_self());
        #endif
		return -1;
	}
	sem_post(semaphore);
	pthread_mutex_unlock(tasks_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_task: Task adicionada com sucesso\n",pthread_self());
    #endif
	return 0;
}

void *listen_select_task (void * arg)
{
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]listen_select_task()\n",pthread_self());
   	#endif
	
	select_task_args_t * parameters = (select_task_args_t*) arg;
	info_socket_t * vec = parameters->vec;
	int  vec_lenght = parameters->vec_lenght;
	info_cmd_queue_t ** tasks = parameters->tasks;
	pthread_mutex_t * vec_mutex = parameters->vec_mutex;
	pthread_mutex_t * tasks_mutex = parameters->tasks_mutex;
	pthread_mutex_t * IOvec_mutex = parameters->IOvec_mutex;
	info_socket_t * IOvec = parameters->IOvec;
	sem_t * tasks_sem = parameters->tasks_sem;
	fd_set listenSet;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]listen_select_task: Inicialização completa\n",pthread_self());
    #endif

	while(running)
	{
		////printf("SV: Nada1\n");
		FD_ZERO(&listenSet);
		long int maxFd=-1;
		int serverActivity;
		////printf("SV: Nada12\n");
		pthread_mutex_lock(vec_mutex);
		////printf("SV: Nada13\n");
		for (int i = 0; i < vec_lenght; ++i)
		{			
			if(vec[i].shutdown == 1 && vec[i].socket!=0)
			{
				#ifdef DEBUG
				syslog(LOG_DEBUG, "[%lu]listen_select_task: Shutting down socket %d\n",pthread_self(),vec[i].socket);
                #endif
				int error = shutdown(vec[i].socket,SHUT_RDWR);
				if(error < 0)
				{
					perror("Erro shutdown() em server select:");
				}
				vec[i].socket = 0;
			}
			if(vec[i].select_ready)
			{
				if(vec[i].socket > maxFd)
				{
					maxFd = vec[i].socket;
				}
				FD_SET(vec[i].socket, &listenSet);
			}
			
		}
		
		pthread_mutex_unlock(vec_mutex);
		
		a.tv_usec = SV_TIMEOUT_SECONDS;


		serverActivity = select( maxFd + 1 , &listenSet , NULL , NULL , &a);
		
		#ifdef DEBUG
		if(serverActivity == 0)
		{
			syslog(LOG_DEBUG, "[%lu]listen_select_task: Nenhuma atividade aconteceu\n",pthread_self());
		}

		if ((serverActivity < 0) && (errno != EINTR)) 
		{
			syslog(LOG_DEBUG, "[%lu]listen_select_task: Ocorreu algum erro no select da threads\n",pthread_self());
		}
        #endif
		for (int i = 0; i < vec_lenght; ++i)
		{
			if( FD_ISSET(vec[i].socket, &listenSet))
			{
				#ifdef DEBUG
				syslog(LOG_DEBUG, "[%lu]listen_select_task: Atividade no socket %d\n",pthread_self(), vec[i].socket);
                #endif
				vec[i].select_ready = false;
				info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));
				if(vec[i].type == TLS_SERVER)
				{
					#ifdef TLS
					new_task->cmd.cmd =&cmd_accept_tls;
					#endif	
				}else
				{
					new_task->cmd.cmd =&cmd_accept;
				}
				new_task->cmd.socket = &vec[i];
				new_task->next =NULL;
				new_task->prev =NULL;
				new_task->id = taskID;
				new_task->cmd.data_slot = malloc(sizeof(accept_socket_data));
				accept_socket_data acceptCmd_args;
				acceptCmd_args.IOvec = IOvec;
				acceptCmd_args.IOvec_mutex = IOvec_mutex;
				memcpy(new_task->cmd.data_slot,&acceptCmd_args,sizeof(accept_socket_data));
				add_task(tasks, new_task, tasks_sem, tasks_mutex);
				taskID++;
			}
		}
	}
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]listen_select_task: Finalizada %d\n",pthread_self());
    #endif
}


void *
IO_select_task (void * arg)
{
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]IO_select_task()\n",pthread_self());
    #endif
	select_task_args_t * parameters = (select_task_args_t*) arg;
	info_socket_t * vec = parameters->IOvec;
	int  vec_lenght = parameters->vec_lenght;
	pthread_mutex_t * vec_mutex = parameters->IOvec_mutex;
	info_cmd_queue_t ** tasks = parameters->tasks;
	pthread_mutex_t * tasks_mutex = parameters->tasks_mutex;
	sem_t * tasks_sem = parameters->tasks_sem;
	fd_set readSet;
	fd_set writeSet;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]IO_select_task: Inicialização completa\n",pthread_self());
    #endif
	while(running)
	{
		if(countSec>0)
		{

		}
		
		FD_ZERO(&readSet);
		FD_ZERO(&writeSet);
		long int maxFd=-1;
		int IOActivity;

		pthread_mutex_lock(vec_mutex);
		for (int i = 0; i < vec_lenght; ++i)
		{	
			if(vec[i].shutdown == 1 && vec[i].socket != 0 && vec[i].socket_data_output.message_queue == (message_queue_t*)NULL)
			{
				#ifdef DEBUG
				syslog(LOG_DEBUG, "[%lu]IO_select_task: Shutting down socket %d   Connected = %d\n",pthread_self(),vec[i].socket,vec[i].isConnected);
                #endif
				if(vec[i].isConnected)
				{	
					int error = close(vec[i].socket);
					if(error < 0)
					{
						/*erro ao desligar o socket*/
						perror("Erro shutdown() em IO select:");
					}
				}
				vec[i].socket = 0;
			}
			if(vec[i].socket!= 0)
			{

				if(vec[i].socket > maxFd)
				{
					maxFd = vec[i].socket;
				}
				if(vec[i].select_ready==true)
				{	
					if(vec[i].shutdown == 0) /*Em caso de desligamento, nenhuma leitura pode ser feita, mas as escritas continuam até o buffer esvaziar*/
					{
						FD_SET(vec[i].socket, &readSet);	
					}
					if(vec[i].inWrite == false)
					{
						if(queue_size((queue_t*)vec[i].socket_data_output.message_queue) >= 1)/*Um socket só necessita saber se pode ser escrito quando tem-se algo para escrever nele que não esta sendo escrito por outra thread*/
						{
							FD_SET(vec[i].socket, &writeSet);
						}
					}
					
				}
			}
			
		}
		pthread_mutex_unlock(vec_mutex);
		
		b.tv_usec = IO_TIMEOUT_SECONDS;
		
		IOActivity = select( maxFd + 1 , &readSet , &writeSet , NULL , &b);
		
		
		if ( (IOActivity < 0) && (errno!=EINTR)) 
		{
			
		}
		
		if(IOActivity == 0)
		{
			
			countSec++;

			syslog(LOG_DEBUG, "[%lu]IO_select_task: Nenhuma atividade aconteceu\n",pthread_self());
		}
		else
		{
			
		////printf("Atividadde IO\n");
			countSec = 0;
			for (int i = 0; i < vec_lenght; ++i)
			{
				if(FD_ISSET(vec[i].socket, &readSet) && FD_ISSET(vec[i].socket, &writeSet))
				{
					//printf("Atividadde IO: read and write\n");
					if(vec[i].inRead == false)
					{
						//printf("\n");
						info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));
						if(vec[i].type ==TLS_IO)
						{
							#ifdef TLS 
							new_task->cmd.cmd =&cmd_read_tls;
							#endif						
						}
						else
						{
							new_task->cmd.cmd =&cmd_read;
						}

						new_task->cmd.socket = &vec[i];
						new_task->next =NULL;
						new_task->prev =NULL;
						new_task->id = taskID;
						new_task->cmd.data_slot = malloc(sizeof(accept_socket_data));
						accept_socket_data acceptCmd_args;
						memcpy(new_task->cmd.data_slot,&acceptCmd_args,sizeof(accept_socket_data));
						vec[i].inRead = true;
						add_task(tasks, new_task, tasks_sem, tasks_mutex);
						
						taskID++;
					}
					if(vec[i].inWrite == false && vec[i].socket_data_output.message_queue != NULL )
					{
						//printf("\n");
						info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));


						if(vec[i].type ==TLS_IO)
						{
							#ifdef TLS 
							new_task->cmd.cmd =&cmd_write_tls;	
							#endif			
						}
						else
						{
							new_task->cmd.cmd =&cmd_write;
						}






						new_task->cmd.socket = &vec[i];
						new_task->next =NULL;
						new_task->prev =NULL;
						new_task->id = taskID;
						vec[i].inWrite = true;
						add_task(tasks, new_task, tasks_sem, tasks_mutex);
						
					}
				}
				else if(FD_ISSET(vec[i].socket, &readSet) && !FD_ISSET(vec[i].socket, &writeSet))
				{
////printf("Atividadde IO: read\n");	
					
					if(vec[i].inRead == false)
					{
						//printf("\n");
						info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));
						if(vec[i].type ==TLS_IO)
						{
							#ifdef TLS 
							new_task->cmd.cmd =&cmd_read_tls;	
							#endif					
						}
						else
						{

							new_task->cmd.cmd =&cmd_read;
						}

						new_task->cmd.socket = &vec[i];
						new_task->next =NULL;
						new_task->prev =NULL;
						new_task->id = taskID;
						taskID++;
						//new_task->cmd.data_slot = malloc(sizeof(accept_socket_data));
						accept_socket_data acceptCmd_args;
						//memcpy(new_task->cmd.data_slot,&acceptCmd_args,sizeof(accept_socket_data));
						vec[i].inRead = true;
						add_task(tasks, new_task, tasks_sem, tasks_mutex);
						//printf(" colocando socket %d em inREAD\n",vec[i].socket);
						
					}
					else
					{

					}
				}
				else if(!FD_ISSET(vec[i].socket, &readSet) && FD_ISSET(vec[i].socket, &writeSet))
				{

					//printf("Atividadde IO: write\n");
					if(vec[i].inWrite == false && vec[i].socket_data_output.message_queue != NULL )
					{
						//printf("\n");


						info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));

						if(vec[i].type ==TLS_IO)
						{
							#ifdef TLS 
							new_task->cmd.cmd =&cmd_write_tls;		
							#endif			
						}
						else
						{
							new_task->cmd.cmd =&cmd_write;
						}


						new_task->cmd.socket = &vec[i];
						new_task->next =NULL;
						new_task->prev =NULL;
						new_task->id = taskID;
						vec[i].inWrite = true;
						add_task(tasks, new_task, tasks_sem, tasks_mutex);
					}
				}
			}
		}
	}


	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]IO_select_task: Finalizada\n",pthread_self());
    #endif
}
