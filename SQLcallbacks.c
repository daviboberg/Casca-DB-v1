
#include <stdio.h>
#include "data_structs.h"
#include <pthread.h>
#include <semaphore.h>
#include "queue.h"
#include "SQLcallbacks.h"
//#include "global_vars.h"
#include <unistd.h>
#include "API.h"


//banco
#include "SQL/src/db.h"
#include "SQL/src/csv_parser.h"


extern sql_req* 	   req_queue;
extern pthread_mutex_t req_queue_mutex;
extern sem_t 		   req_queue_sem;
extern int             running;
extern char*		   url;
extern char*           apiKey;



void stop_all_SQL()
{
	running =0;


}

void print_elem22 (void *ptr)
{
   sql_req *elem = ptr ;

   if (!elem)
      return ;

   elem->prev ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
   printf ("<%d>", elem->outputsocket->socket) ;
   elem->next ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
}

long SQL_create_req(void* isocket,void* msg, long tam)
{
	
	//printf("%s", aux);
	regex_t sqlPattern;
    regcomp(&sqlPattern,"(\\s*)?([a-zA-Z0-9]+)?", REG_EXTENDED);

    char* word = NULL;
    int type = -1;
    int matchOperation = get_match(&sqlPattern, msg, &word, 0);
    if(matchOperation == REG_NOERROR)
    {
        char* operation = substring(word,0,strlen(word),' ');

        if(strcasecmp(operation, "SELECT") == 0)
        {
            type = SQL_SELECT;
        }
        else if(strcasecmp(operation, "CREATE") == 0)
        {
            char* secondWord = NULL;
            int matchOperation2 = get_match(&sqlPattern, msg + strlen(word), &secondWord, 0);
            if(matchOperation2 == REG_NOERROR)
            {
                char* operation2 = substring(secondWord,0, strlen(secondWord), ' ');
                if(strcasecmp(operation2, "TABLE") == 0)
                    type = SQL_CREATE_TABLE;
                free(operation2);
            }
            free(secondWord);

        }
        else if(strcasecmp(operation, "INSERT") == 0)
        {
            type = SQL_INSERT;
        }
        else if(strcasecmp(operation, "DELETE") == 0)
        {
            type = SQL_DELETE;
        }
        else
            type = SQL_NO_MATCH;
        free(operation);
    }
    regfree(&sqlPattern);
    free(word);

    if(type != -1 && type != SQL_NO_MATCH)
    {
    	sql_req* new_req = (sql_req* )calloc(1,sizeof(sql_req));
    	new_req->prev =NULL;
		new_req->next =NULL;


		info_socket_t* current_socket = (info_socket_t*) isocket;
		new_req->outputsocket = current_socket;


		new_req->sql_statement = (char*)calloc(tam,sizeof(char));
		memcpy(new_req->sql_statement, msg, tam-2);
		new_req->sql_size = tam-1;


		add_req(new_req);
		//printf("mensagem recortada:\n%s\n",new_req->sql_statement );
		return tam-1;
    }	
    //Provavelmente tem ccomo fazer isso melhor
    bool discart = false;
    char* aux = msg;
    for(int i = 0; i < tam; i++)
    {
    	if(aux[i] == ';')
    	{
    		discart = true;
    		break;
    	}
    }
    if(discart || tam > 9000)
    {
    	//printf("Preciso descartar essa mensagem\n");
    	return -1;
    }
    return 0;
			

    
}

/*long SQL_create_req(void* isocket,void* msg, long tam)
{
	//If size of the msg is lower than 10 character is invalid
	if(tam<10)
	{
		return 0;
	}

	//Adding an \0 at the and of the msg
	void* msg2 = calloc( (tam+2),sizeof(char));
	memcpy(msg2,msg,tam);
	char * aux = msg2;
	aux[tam+1] ='\0';


	info_socket_t* current_socket = (info_socket_t*) isocket;
	int init = 0;
	int fondinit = 0;
	int count1=0;
	int count2=0;
	int countSpaces= 0;
	int isSelect = 0;
	int len = strlen(aux);

	for (int i = 0;i<len; ++i)
	{			

		if( (aux[i]=='I'||aux[i]=='i'||aux[i]=='S'||aux[i]=='s'||aux[i]=='C'||aux[i]=='C') && fondinit == 0)
		{
			init = i;
			fondinit=1;
			if(aux[i]=='S'||aux[i]=='s')
			{
				isSelect = 1;
			}
		}
		else if(aux[i]=='(')
		{

			count1++;
		}
		else if(aux[i]==')')
		{

			count2++;
		}
		if(isSelect == 1)
		{
			if(aux[i]==' ')
			{
				countSpaces++;
				if (countSpaces >=4)
				{
					count1++;
					count2++;
				}
			}

		}

		if((count1 == count2) && count1>0)
		{
			sql_req* new_req = (sql_req* )calloc(1,sizeof(sql_req));
			new_req->prev =NULL;
			new_req->next =NULL;
			new_req->outputsocket =current_socket;
			new_req->sql_statement = (char*)calloc(i+2,sizeof(char));
			memcpy(new_req->sql_statement, &(aux[init]), i+1);
			new_req->sql_size = i+2;
			add_req(new_req);
			free(msg2);
			printf("mensagem recortada:\n%s\n",new_req->sql_statement );
			

			return i+1;


		}


	}	
	return 0;
}*/


int add_req (sql_req* req_to_add )
{

#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_req()\n",pthread_self()%100);
    #endif
	if(req_to_add == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: req nula, abortando operação\n",pthread_self()%100);
        #endif
		return -1;
	}
	if(&req_queue_sem == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Semaforo nulo, abortando operação\n",pthread_self()%100);
        #endif
		return -1;
	}
	if(&req_queue_mutex == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Mutex nulo, abortando operação\n",pthread_self()%100);
        #endif
		return -1;
	}
	pthread_mutex_lock(&req_queue_mutex);
	
	int err = queue_append ((queue_t **) &req_queue, (queue_t*) req_to_add);
	
	if(err<0)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Erro na inserção na fila, abortando operação\n",pthread_self()%100);
        #endif
		return -1;
	}
	sem_post(&req_queue_sem);
	pthread_mutex_unlock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_req: req adicionada com sucesso\n",pthread_self()%100);
    #endif
	return 0;




}





void* do_req()
{


	int num_reqs;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_req()\n",pthread_self()%100);
    #endif

	sql_req* current_req = NULL;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_req: Parametros recebidos\n",pthread_self()%100);
    #endif
	/*Loop de busca e execução de comandos*/
	do
	{
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Aguardando req\n",pthread_self()%100);
        #endif
		sem_wait(&req_queue_sem);
			
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Req detectada na fila\n",pthread_self()%100);
        #endif
		current_req =get_req(current_req);
		
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Req recebida\n",pthread_self()%100);
        #endif
		if(current_req != NULL)
		{
			#ifdef DEBUG
			syslog(LOG_DEBUG, "[%lu]do_req: Req não nula, executando comando\n",pthread_self()%100);
            #endif

			DBConnection* connection;

		    RESTRequest* request = restRequest_init();
		   
		    char dBapiKey[256];
			sprintf(dBapiKey,"Authorization: ApiKey %s\r\n",apiKey);
		   
		    request = restRequest_setUrl(request, url);
		    request = restRequest_appendHeader(request, dBapiKey);
		    request = restRequest_appendHeader(request, "Content-Type: application/json");

		    connection = db_connectRest("./SQL/database.db", request);

		    char* buf;
		  
		    buf = calloc(10000000,sizeof(char));
		    syslog(LOG_INFO, "Iniciando query recebida do ip %s \n",inet_ntoa(current_req->outputsocket->info.sin_addr) );
		    query(connection, current_req->sql_statement,buf); //,buf pedro
		  
		    syslog(LOG_INFO, "Query finalizada, enviando resultado \n");
		 	
			if(strlen(buf)>0)
			{
				API_write(current_req->outputsocket,(void*)current_req->sql_statement,strlen(current_req->sql_statement),NULL);
				API_write(current_req->outputsocket,(void*) buf,strlen(buf),NULL);
			}else
			{
				API_write(current_req->outputsocket,(void*)"Query invalida\n",15,NULL);
			}
			free(buf);
			#ifdef DEBUG
				syslog(LOG_DEBUG, "[%lu]do_req: Req concluida\n",pthread_self()%100);
			#endif
				
		}
	sem_getvalue(&req_queue_sem,  &num_reqs);
	free(current_req->sql_statement);

	free(current_req);
		
	}while(running == 1 );
}





sql_req*
get_req(sql_req* current_req)
{

	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req()\n",pthread_self()%100);
	syslog(LOG_DEBUG, "[%lu]get_req: Aguardando task\n",pthread_self()%100);
    #endif
	pthread_mutex_lock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req: Req detectada na fila\n",pthread_self()%100);
    #endif
	if(&req_queue!= NULL)
	{
		
		current_req = (sql_req *) queue_remove( (queue_t **)&req_queue, (queue_t *)req_queue);
	
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_req: Req retirada da fila\n",pthread_self()%100);
        #endif
	} 
	else
	{
		current_req = NULL;
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_req: Erro, req nula\n",pthread_self()%100);
        #endif
	}
	pthread_mutex_unlock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req: req retirada da fila com sucesso\n",pthread_self()%100);
    #endif
    return current_req;
}
