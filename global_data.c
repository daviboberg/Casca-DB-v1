#include <pthread.h>
#include <semaphore.h>
#include "data_structs.h"
#include "API.h"

#include <regex.h>

#include <syslog.h>

/* vetor de sockets para processamento */


#include "global_vars.h"

extern int var;

void print_elem (void *ptr)
{
	sql_req *elem = ptr ;

	if (!elem)
		return ;

	elem->prev ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
	printf ("<%d>", elem->outputsocket->socket) ;
	elem->next ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
}


void create_dummy_task_army(int amount)
{
	for (int i = 0; i < amount; ++i)
	{
		API_create_dummy_task();
	}

}

long process_string(void*  socket,void* msg, long tam)
{
	//syslog(LOG_WARNING, "process_default: Essa função é um placeholder");
	int rv;
	int result;
	info_socket_t* iS = (info_socket_t*) socket;
	regex_t exp; //Our compiled expression
	char * aux = (char*)msg;
	
	//Insert into reading ('SDT-LNA-5329310-EDITORA E GRAFICA PARANA PRESS S.A.','15/07/2016 00:00','Sexta','128,00','129,00','129,00','0,00','240,00','120,00','1,94','1,88','1,98','311,00','193,00','77,00','163,00','166,00','184,00','0,65','0,68','0,72','223,00','224,00','223,00','1,00','60,00','123','462,00','472,00','2,00','49.200.956,00','24.107.156,00','3.882.651,00')
	rv = regcomp(&exp, "([i,I][n,N][s,S][e,E][r,R][t,T])\\s([i,I][n,N][t,T][o,O].*)\\s?(\\s)([a-zA-Z0-9\\-_\\.]+)?(\\s)", REG_EXTENDED);
	

	result = regexec (&exp, aux, 0, NULL, 0);
	if (!result)
	{
		char ans[] = "{\"rows\" : 1,\"status\" : \"OK\", \"message\" : \" \" ,\"results\" :  \" \"}\n";
		API_write(iS,ans,strlen(ans),NULL);
		return 1;
	}
	else
	{
		char ans[] = "{\"rows\" : 0,\"status\" : \"ERROR\", \"message\" : \"Insert invalido \" ,\"results\" :  \" \"}\n";
		API_write(iS,ans,strlen(ans),NULL);
		return 0;
	}




	
}




int main(int argc, char * argv[])
{



	openlog("log", LOG_PERROR, LOG_USER);
	setlogmask (LOG_MASK(LOG_INFO)|LOG_MASK(LOG_ERR));
	//setlogmask (LOG_MASK(LOG_ERR)|LOG_MASK(LOG_WARNING)|LOG_MASK(LOG_INFO));
	config_struct_t config;
	

	use_getopt(&argc,  &argv, &config);
	
	
	API_Init(config);
	

	
	/*---------------------------------------------VArea de testesV------------------------------------------------------*/
	API_create_SQLserver(config.sql_port);

	info_socket_t* a;
	#ifdef TLS
		a = API_create_server_TLS(4434, NULL, &process_string);
	
		if(a == NULL )
		{	
			return -1;		
		}
	#endif
	a = API_create_server(4435, NULL, &process_string);
	if(a == NULL )
	{	
		return -1;		
	}

	while(1)
	{
		
		sleep(1500000);	
	}
	


	
	//API_close_socket(iS);
	API_shutdown();
	/*--------------------------------------------^Area de testes^-----------------------------------------------------*/



	closelog();	
	printf("FIM main\n");

	return 0;
}
