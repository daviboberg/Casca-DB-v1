#include "results.h"

Row* row_create(QueryTable* queryTable, char* keys, char* values)
{
    char** keysFields = NULL;
    int nKeys = 0;
    if(keys == NULL)
        nKeys = 0;
    else
    {
        keysFields = getAllFields(keys);
        nKeys = getNumberOfFields(keys);
    }

    char** valuesFields = NULL;
    int nValues = 0;
    // Indice dos atributos que não sao PK
    int* noPKIndexes = NULL;
    int noPKIndex = 0;
    if(values == NULL)
        nValues = 0;
    else
    {
        valuesFields = getAllFields(values);
        nValues = getNumberOfFields(values);

        noPKIndexes = malloc(sizeof(int)*nValues);
        int pks[queryTable->header->length];
        for(int i = 0; i < queryTable->header->length; i++)
            pks[i] = i;
        for(int i = 0; i < queryTable->numberOfPrimaryKeys; i++)
        {
            pks[queryTable->primaryKeysIndex[i]] = -1;
        }

        for(int i = 0; i < queryTable->header->length; i++)
        {
            if(pks[i] >= 0)
            {
                noPKIndexes[noPKIndex] = i;
                noPKIndex++;
            }
        }
    }

    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*queryTable->header->length);
    for(int i = 0; i < queryTable->header->length; i++)
        newRow->values[i] = NULL;

    // começa em 1 pois tem o nome da tabela na primeira posição
    for(int i = 1; i < nKeys; i++)
    {
        newRow->values[queryTable->primaryKeysIndex[i-1]] = keysFields[i];
    }
    free(keysFields[0]);

    for(int i = 0; i < nValues; i++)
    {
        newRow->values[noPKIndexes[i]] = valuesFields[i];
    }
    newRow->length = queryTable->header->length;

    if(noPKIndexes != NULL)
        free(noPKIndexes);
    if(valuesFields != NULL)
        free(valuesFields);
    if(keysFields != NULL)
        free(keysFields);

    return newRow;
}

Row* row_copy(Row* row)
{
    if(row == NULL)
    {
        printf("Invalid row.\n");
        return NULL;
    }

    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*row->length);
    for(int i = 0; i < row->length; i++)
    {
        int size = strlen(row->values[i]);
        newRow->values[i] = malloc(sizeof(char)*(size+1));
        strncpy(newRow->values[i], row->values[i], size);
        newRow->values[i][size] = '\0';
    }
    newRow->length = row->length;
    return newRow;
}

// Return a new Row modified
Row* row_modify(Row* original, int* indexesToSave, int nIndexes)
{
    if(original == NULL)
    {
        printf("Invalid Row.\n");
        return NULL;
    }
    if(indexesToSave == NULL)
    {
        printf("invalid indexes.\n");
        return NULL;
    }

    if(nIndexes < 0 || nIndexes > original->length)
    {
        printf("Invalid number os indexes.\n");
        return NULL;
    }


    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*nIndexes);

    int saveIndex = 0;
    for(int i = 0; i < original->length; i++)
    {
        for(int j = 0; j < nIndexes; j++)
        {
            if(indexesToSave[j] == i)
            {
                int size = strlen(original->values[i]);
                newRow->values[saveIndex] = malloc(sizeof(char)*(size+1));
                strncpy(newRow->values[saveIndex], original->values[i], size);
                newRow->values[saveIndex][size] = '\0';
                saveIndex++;
            }
        }
    }

    newRow->length = nIndexes;

    return newRow;
}

void row_free(Row* row)
{
    if(row == NULL)
        return;

    if(row->values == NULL)
        return;

    for(int i = 0; i < row->length; i++)
        free(row->values[i]);
    free(row->values);
    row->values = NULL;
    row->length = 0;
    free(row);
    row = NULL;
    return;
}

QueryTable* queryTable_addRow(QueryTable* queryTable, Row* row)
{
    Row** newRow = malloc(sizeof(Row));
    newRow[0] = row;
    QueryTable* newQueryTable = queryTable_addNRows(queryTable, newRow, 1);
    free(newRow);
    return newQueryTable;
}


QueryTable* queryTable_addNRows(QueryTable* queryTable, Row** rows, int nRows)
{
    int newSize;
    int size;

    if(queryTable == NULL)
    {
        printf("QueryTable does not exist\n");
        return NULL;
    }

    newSize = queryTable->numberOfRows + nRows;
    size = queryTable->numberOfRows;
    Row** newRows;
    if(queryTable->rows == NULL || size == 0)
    {
        newRows = malloc(sizeof(Row*)*nRows);
    }
    else
        newRows = (Row**)realloc(queryTable->rows, sizeof(Row*)*newSize);

    if(newRows  == NULL) {
        //out of memory!
        printf("\nFailed to realloc, Row not add to QueryTable\n");
        return NULL;
    }

    queryTable->rows = newRows;
    for(int i = 0; i < nRows; i++)
    {
        queryTable->rows[size + i] = rows[i];
    }
    queryTable->numberOfRows = newSize;
    free(rows);

    return queryTable;
}

QueryTable* queryTable_copyNRowsToQueryTable(QueryTable* queryTable, Row** rows, int nRows)
{
    if(queryTable == NULL)
    {
        printf("QueryTable does not exist\n");
        return NULL;
    }

    if(rows == NULL)
    {
        printf("\nrows does not exist.\n");
    }

    Row** newRows = malloc(sizeof(Row*)*nRows);

    for(int i = 0; i < nRows; i++)
    {
        newRows[i] = row_copy(rows[i]);
    }
    queryTable = queryTable_addNRows(queryTable, newRows, nRows);

    return queryTable;
}

QueryTable* queryTable_create(Table* table, Row** rows, int nRows)
{
    if(table == NULL)
    {
        printf("Invalid table.\n");
        return NULL;
    }

    QueryTable* newQueryTable = malloc(sizeof(QueryTable));

    int nameSize = strlen(table->name);
    newQueryTable->tableName = malloc(sizeof(char)*(nameSize+1));
    strncpy(newQueryTable->tableName, table->name, nameSize);
    newQueryTable->tableName[nameSize] = '\0';

    newQueryTable->header = malloc(sizeof(Row));
    newQueryTable->header->values = copyArrayOfString(table->attributes, table->numberOfAttributes);
    newQueryTable->header->length = table->numberOfAttributes;

    newQueryTable->type = malloc(sizeof(Row));
    newQueryTable->type->values = copyArrayOfString(table->attributeType, table->numberOfAttributes);
    newQueryTable->type->length = table->numberOfAttributes;

    newQueryTable->primaryKeysIndex = malloc(sizeof(int)*table->numberOfPrimaryKeys);
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {
        newQueryTable->primaryKeysIndex[i] = table->indexPrimaryKeys[i];
    }
    newQueryTable->numberOfPrimaryKeys = table->numberOfPrimaryKeys;

    newQueryTable->numberOfRows = 0;
    newQueryTable->rows = NULL;



    if(rows == NULL || nRows <= 0)
        return newQueryTable;


    newQueryTable = queryTable_addNRows(newQueryTable, rows, nRows);

    return newQueryTable;
}

QueryTable* queryTable_copy(QueryTable* queryTable)
{
    if(queryTable == NULL)
    {
        printf("Invalid QueryTable.\n");
        return NULL;
    }

    QueryTable* newQueryTable = malloc(sizeof(QueryTable));

    int nameSize = strlen(queryTable->tableName);
    newQueryTable->tableName = malloc(sizeof(char)*(nameSize+1));
    strncpy(newQueryTable->tableName, queryTable->tableName, nameSize);
    newQueryTable->tableName[nameSize] = '\0';

    newQueryTable->header = malloc(sizeof(Row));
    newQueryTable->header->values = copyArrayOfString(queryTable->header->values, queryTable->header->length);
    newQueryTable->header->length = queryTable->header->length;

    newQueryTable->type = malloc(sizeof(Row));
    newQueryTable->type->values = copyArrayOfString(queryTable->header->values, queryTable->header->length);
    newQueryTable->type->length = queryTable->header->length;

    newQueryTable->primaryKeysIndex = malloc(sizeof(int)*queryTable->numberOfPrimaryKeys);
    for(int i = 0; i < queryTable->numberOfPrimaryKeys; i++)
    {
        newQueryTable->primaryKeysIndex[i] = queryTable->primaryKeysIndex[i];
    }
    newQueryTable->numberOfPrimaryKeys = queryTable->numberOfPrimaryKeys;

    newQueryTable->numberOfRows = 0;
    newQueryTable->rows = NULL;



    if(queryTable->rows == NULL || queryTable->numberOfRows <= 0)
        return newQueryTable;


    newQueryTable = queryTable_copyNRowsToQueryTable(newQueryTable, queryTable->rows, queryTable->numberOfRows);

    return newQueryTable;
}


QueryTable* queryTable_modifyHeader(QueryTable* queryTable, int* indexes, int numberOfIndexes)
{
    if(queryTable == NULL)
    {
        printf("Invalid queryTable.\n");
        return NULL;
    }

    if(indexes == NULL)
    {
        printf("Invalid indexes.\n");
        return NULL;
    }

    if(numberOfIndexes < 0)
    {
        printf("Invalid number of indexes.\n");
        return NULL;
    }


    int newPrimaryKeys[numberOfIndexes];
    int numberOfPKs = 0;
    Row* newHeader = malloc(sizeof(Row));
    newHeader->values = malloc(sizeof(char*)*numberOfIndexes);
    for(int i = 0; i < numberOfIndexes; i++)
    {
        for(int j = 0; j < queryTable->numberOfPrimaryKeys; j++)
        {
            if(indexes[i] == queryTable->primaryKeysIndex[j])
            {
                newPrimaryKeys[numberOfPKs] = i;
                numberOfPKs++;
            }
        }
    }

    for(int i = 0; i < queryTable->header->length; i++)
        free(queryTable->header->values[i]);
    free(queryTable->header);

    newHeader = row_modify(queryTable->header, indexes, numberOfIndexes);
    row_free(queryTable->header);
    queryTable->header = newHeader;
    queryTable->header->length = numberOfIndexes;

    if(queryTable->numberOfPrimaryKeys != numberOfPKs)
    {
        free(queryTable->primaryKeysIndex);
        queryTable->primaryKeysIndex = malloc(sizeof(int)*numberOfPKs);
    }

    for(int i = 0; i < numberOfPKs; i++)
    {
        queryTable->primaryKeysIndex[i] = newPrimaryKeys[i];
    }

    return queryTable;
}


QueryTable* queryTable_setHeader(QueryTable* queryTable, Row* newHeader)
{
    if(queryTable == NULL)
    {
        printf("Invalid query table.\n");
        return NULL;
    }
    if(newHeader == NULL)
    {
        printf("Invalid newHeader.\n");
        return NULL;
    }

    if(queryTable->header != NULL)
    {
        for(int i = 0; i < queryTable->header->length; i++)
        {
            free(queryTable->header->values[i]);
        }
        free(queryTable->header);
    }

    queryTable->header = newHeader;

    return queryTable;
}

void queryTable_print(QueryTable* queryTable)
{
    if(queryTable == NULL)
        return;

    if(queryTable->header == NULL)
        return;

    printf("Row ");
    for(int i = 0; i < queryTable->header->length; i++)
    {
        printf("%s ", queryTable->header->values[i]);
    }


    if(queryTable->rows == NULL)
    {
        printf("\n");
        return;
    }

    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        printf("\n => %d ", i);
        for(int j = 0; j < queryTable->rows[i]->length; j++)
        {
            if(queryTable->rows[i]->values[j] != NULL)
                printf("%s ", queryTable->rows[i]->values[j]);
            else
                printf("-- ");
        }

    }
    printf("\n");
    return;
}

void results_free(Results* result)
{
    if(result == NULL)
        return;

    if(result->results != NULL)
        queryTable_free(result->results);

    if(result->message != NULL)
        free(result->message);

    free(result);
    return;
}

void queryTable_free(QueryTable* queryTable)
{
    if(queryTable == NULL)
        return;

    if(queryTable->primaryKeysIndex != NULL)
        free(queryTable->primaryKeysIndex);

    if(queryTable->header != NULL)
    {
        row_free(queryTable->header);
    }

    if(queryTable->type != NULL)
        row_free(queryTable->type);

    if(queryTable->rows != NULL)
    {
        for(int i = 0; i < queryTable->numberOfRows; i++)
            row_free(queryTable->rows[i]);
        free(queryTable->rows);
        queryTable->rows = NULL;
    }

    if(queryTable->tableName != NULL)
        free(queryTable->tableName);

    free(queryTable);
    queryTable = NULL;
    return;
}
