#include "sql_select.h"

//#define DEBUG_RESULTS
//#define SHOW_RESULTS
//#define PRINT_TABLES
//#define DEBUG_FINAL_RESULTS

Results* sql_select(DBConnection* connection, char* sql)
{
    // Parse sql
    consultaSelect* consulta;
    consulta = parse_sql_select(sql);
    //print_consulta_select(*consulta);

    Results* select_results;
    select_results = malloc(sizeof(Results));
    select_results->message = NULL;
    select_results->status = "ERROR";
    select_results->affected_rows = 0;
    select_results->results = NULL;


    QueryTable* queryTable = NULL;
    QueryTable* tempQueryTable = NULL;

    Table* table = NULL;


    // Verifica se a tabela da consulta existe no banco de dados
    table = findTable(connection->database, consulta->tables[0]);
    if(table == NULL)
    {
        select_results->message = stringConcat(select_results->message, "Table ");
        select_results->message = stringConcat(select_results->message, consulta->tables[0]);
        select_results->message = stringConcat(select_results->message, " not found");
        select_results->status = "ERROR";
        select_results->affected_rows = 0;
        select_results->results = NULL;
        return select_results;

    }

    int attributePK = 0; // Indica se todos os atributos são Primary Keys
    int conditionPK = 0; // Indica se todos as condições são Primary Keys
    int selectAll = 0;   // Indica se é Select *

    if(consulta->numberOfAttributes == 1)
    {
        if(strncmp(consulta->attributes[0], "*", strlen("*")) == 0)
        {
            selectAll = 1;
        }
    }

    // verifica se todos os atributos são primary keys.
    for(int i = 0; i < consulta->numberOfAttributes; i++)
    {
        for(int j = 0; j < table->numberOfPrimaryKeys; j++ )
        {
            attributePK = 0;
            if(strncmp(table->attributes[table->indexPrimaryKeys[j]], consulta->attributes[i], strlen(table->attributes[j])) == 0 )
            {
                attributePK = 1;
                break;
            }
        }

        if(attributePK == 0)
            break;
    }



    // Verifica se todas as condições pertencem as Primary Keys

    // Se for igual a zero, basta salvar os atributos selecionados
    if(consulta->numberOfConditions > 0)
    {
        // Verifica se todas as condições são primary keys.
        for(int i = 0; i < consulta->numberOfConditions; i++)
        {
            for(int j = 0; j < table->numberOfPrimaryKeys; j++)
            {
                conditionPK = 0;
                if(strncmp(table->attributes[table->indexPrimaryKeys[j]], consulta->conditions[i][0], strlen(table->attributes[j])) == 0 )
                {
                    conditionPK = 1;
                    break;
                }
            }

            if(conditionPK == 0)
                break;

            int attributeIndex2 = table_getAttributeIndex(table, consulta->conditions[i][2]);
            // Se a segunda condição for uma coluna da tabela
            if(attributeIndex2 >= 0)
            {
                for(int j = 0; j < table->numberOfPrimaryKeys; j++)
                {
                    conditionPK = 0;
                    if(attributeIndex2 == table->indexPrimaryKeys[j])
                    {
                        conditionPK = 1;
                        break;
                    }
                }
                if(conditionPK == 0)
                    break;
            }
        }
    }
    else
        conditionPK =1;



    // Verifica otimizacao de buscar por prefixo
    if(conditionPK == 1)
    {
        int nConditions = 0;
        for(int i = 0; i < consulta->numberOfConditions; i++)
        {
            if(nConditions != i)
                break;
            for(int j = 0; j < table->numberOfPrimaryKeys; j++)
            {
                int strSize = strlen(table->primaryKeys[j]);
                if(strncmp(table->primaryKeys[j], consulta->conditions[i][0], strSize) == 0)
                {
                    nConditions++;
                    break;
                }
            }
        }

        if(nConditions == consulta->numberOfConditions)
        {
            // da para pegar algum prefixo
        }
    }


    // Se attributePK = 1, signfica que todos os atributos da seleção são PrimaryKeys
    // Se attributePK = 0, significa que algum atributo não é PK ou é Select *
    // Se conditionPK = 1, todas as condições que não sao literais são da PK
    // Se conditionPK = 0, alguma ou todas as condições não são PK

    // Se os dois são compostos apenas de PK, significa que só é necessário pegar do banco as keys
    if(attributePK == 1 && conditionPK == 1)
    {

        QueryTable* tempQueryTableProjection;


        tempQueryTable = queryTable_create(table, NULL, 0);
        tempQueryTable = projection(tempQueryTable, table->primaryKeys, table->numberOfPrimaryKeys);

        queryTable = queryTable_copy(tempQueryTable);
        queryTable = projection(queryTable, consulta->attributes, consulta->numberOfAttributes);

#ifdef DEBUG_RESULTS
            printf("\nNew Header.\n");
            queryTable_print(queryTable);
#endif

        do
        {
            // Pega chaves do banco
            tempQueryTable = getKeys(connection, tempQueryTable, consulta->tables[0]);
#ifdef DEBUG_RESULTS
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif

            tempQueryTable = selection(tempQueryTable, consulta->conditions, consulta->numberOfConditions);

#ifdef DEBUG_RESULTS
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            tempQueryTableProjection = queryTable_copy(tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;


            tempQueryTableProjection = projection(tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
#ifdef DEBUG_RESULTS
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTableProjection);
#endif

            queryTable = queryTable_addNRows(queryTable, tempQueryTableProjection->rows, tempQueryTableProjection->numberOfRows);

            tempQueryTableProjection->rows = NULL;
            queryTable_free(tempQueryTableProjection);

#ifdef DEBUG_RESULTS
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

#ifdef DEBUG_FINAL_RESULTS
        printf("\n\nTabela final.\n\n");
        queryTable_print(queryTable);
#endif

        queryTable_free(tempQueryTable);

    }

    // Se algum atributo não é PK, mas as condições são, podemos fazer a
    else if(attributePK == 0 && conditionPK == 1)
    //seleção das linhas apenas utilizando as chaves, e depois pegar somente os valores ques corresponderem àquelas chave
    {
        QueryTable* tempQueryTableProjection;
        tempQueryTable = queryTable_create(table, NULL, 0);
        queryTable = queryTable_copy(tempQueryTable);

        if(selectAll == 0)
            queryTable = projection(queryTable, consulta->attributes, consulta->numberOfAttributes);
        else
            queryTable = projection(queryTable, table->attributes, table->numberOfAttributes);


#ifdef DEBUG_RESULTS
        printf("\nNew Header.\n");
        queryTable_print(queryTable);
#endif

        do
        {

            // Pega chaves do banco
            tempQueryTable = getKeys(connection, tempQueryTable, consulta->tables[0]);
#ifdef DEBUG_RESULTS
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif


            tempQueryTable = selection(tempQueryTable, consulta->conditions, consulta->numberOfConditions);
#ifdef DEBUG_RESULTS
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            tempQueryTable = getValues(connection, tempQueryTable);
#ifdef DEBUG_RESULTS
            printf("\n\nPegou valores.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            tempQueryTableProjection = queryTable_copy(tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;


            if(selectAll == 0)
                tempQueryTableProjection = projection(tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
            else
                tempQueryTableProjection = projection(tempQueryTableProjection, table->attributes, table->numberOfAttributes);
#ifdef DEBUG_RESULTS
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            queryTable = queryTable_addNRows(queryTable, tempQueryTableProjection->rows, tempQueryTableProjection->numberOfRows);
            tempQueryTableProjection->rows = NULL;
            queryTable_free(tempQueryTableProjection);

#ifdef DEBUG_RESULTS
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

        queryTable_free(tempQueryTable);

    }

    else
    {
        QueryTable* tempQueryTableProjection;
        tempQueryTable = queryTable_create(table, NULL, 0);
        queryTable = queryTable_copy(tempQueryTable);
        if(selectAll == 0)
            queryTable = projection(queryTable, consulta->attributes, consulta->numberOfAttributes);
        else
            queryTable = projection(queryTable, table->primaryKeys, table->numberOfPrimaryKeys);

#ifdef DEBUG_RESULTS
        printf("\nNew Header.\n");
        queryTable_print(queryTable);
#endif

        do
        {
            // Pega chaves do banco
            tempQueryTable = getKeys(connection, tempQueryTable, consulta->tables[0]);

#ifdef DEBUG_RESULTS
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif
            tempQueryTable = getValues(connection, tempQueryTable);

#ifdef DEBUG_RESULTS
            printf("\n\nPegou valores.\n\n");
            queryTable_print(tempQueryTable);
#endif

            tempQueryTable = selection(tempQueryTable, consulta->conditions, consulta->numberOfConditions);

#ifdef DEBUG_RESULTS
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            tempQueryTableProjection = queryTable_copy(tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;

            if(selectAll == 0)
                tempQueryTableProjection = projection(tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
            else
                tempQueryTableProjection = projection(tempQueryTableProjection, table->attributes, table->numberOfAttributes);

#ifdef DEBUG_RESULTS
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTableProjection);
#endif
            queryTable = queryTable_addNRows(queryTable, tempQueryTableProjection->rows, tempQueryTableProjection->numberOfRows);
            tempQueryTableProjection->rows = NULL;
            queryTable_free(tempQueryTableProjection);


#ifdef DEBUG_RESULTS
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

#ifdef DEBUG_FINAL_RESULTS
        printf("\n\nTabela final até o momento.\n\n");
        queryTable_print(queryTable);
#endif

        queryTable_free(tempQueryTable);

    }



#ifdef PRINT_RESULTS_SELECT
    printf("\n\nFinal QueryTable.\n\n");
    queryTable_print(queryTable);
#endif

    freeTable(table);
    consultaSelect_free(consulta);

    select_results->message = NULL;
    select_results->status = "OK";
    select_results->affected_rows = queryTable->numberOfRows;
    select_results->results = queryTable;

    // O resultado do select esta em select_result
    char* select_result = JSONFromResults(select_results);

#ifdef PRINT_TABLES

    printf("\n\nSelect Results:\n\n %s", select_result);
#endif //PRINT_TABLES
    free(select_result);


    return select_results;
}

QueryTable* separeteKeys(QueryTable* queryTable, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE], int nKeysToGet, int toGET[MAX_DATA_CURSOR])
{
    Row** rows = malloc(sizeof(Row*)*nKeysToGet);
    int nRows = 0;

    for(int i = 0; i < nKeysToGet; i++)
    {
        if(keys[toGET[i]] != NULL)
        {
            rows[i] = row_create(queryTable, keys[toGET[i]], NULL);
            nRows++;
        }
    }

    queryTable = queryTable_addNRows(queryTable, rows, nRows);
    return queryTable;
}

// Adiciona os valores obtidos nessa consulta a QueryTable
QueryTable* getValues(DBConnection* connection, QueryTable* queryTable)
{
    //char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE], int nKeysToGET, int toGET[MAX_DATA_CURSOR]
    if(queryTable->numberOfRows == 0)
        return queryTable;
    MemoryStruct chunk;
    Row** rows = malloc(sizeof(Row*)*queryTable->numberOfRows);
    int nRows = 0;

    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        char* key = NULL;
        key = stringAppend(key, queryTable->tableName);
        
        for(int j = 0; j < queryTable->numberOfPrimaryKeys; j++)
        {

            key = stringAppend(key , ";");
            key = stringAppend(key, queryTable->rows[i]->values[queryTable->primaryKeysIndex[j]]);
            
        }

        // Replace invalid caracters
        char* valid_key = format_key(key);

        char* urlKey = stringAppend(valid_key, "/");
        char* url = stringConcat(connection->request->url, urlKey);

        chunk.memory = calloc(1,1);  // will be grown as needed by the realloc above
        chunk.size = 0;    // no data at this point

        RESTRequest* newRequest = restRequest_copy(connection->request);
        newRequest = restRequest_setUrl(newRequest, url);

        GET(newRequest, &chunk);

        #ifdef PRINT_RESULTS_SELECT
            printf("\nKey: %s\n", keys[toGET[i]]);
            printf("value: %s \n", chunk.memory);
        #endif

        int nValues = queryTable->header->length - queryTable->numberOfPrimaryKeys;
        if(nValues > 0)
        {
            char* values = getValuesFromJSON(key, chunk.memory, nValues);
            rows[i] = row_create(queryTable, key, values);
            nRows++;
            free(values);
        }

        free(chunk.memory);
        free(urlKey);
        free(key);
        free(url);


    }
    for(int i = 0; i < queryTable->numberOfRows; i++)
        row_free(queryTable->rows[i]);
    free(queryTable->rows);
    queryTable->rows = NULL;
    queryTable->numberOfRows = 0;

    if(nRows > 0)
        queryTable = queryTable_addNRows(queryTable, rows, nRows);

    return queryTable;
}

QueryTable* getKeys(DBConnection* connection, QueryTable* queryTable, char* table)
{

    char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE];
    MemoryStruct chunk;
    chunk.memory = calloc(1,1);
    chunk.size = 0;

    char* urlKey = NULL;
    char* url = NULL;
    if(connection->request->cursor != NULL)
    {
        urlKey = stringConcat("?cursor=", connection->request->cursor);
    }
    else
    {
        urlKey = stringConcat("?key_prefix=", table);
    }
    url = stringConcat(connection->request->url, urlKey);

#ifdef DEBUG_RESULTS
   // printf("url: %s\n\n", url);
#endif //DEBUG_RESULTS

    RESTRequest* newRequest = restRequest_copy(connection->request);
    newRequest = restRequest_setUrl(newRequest, url);

    GET(newRequest, &chunk);

    
#ifdef DEBUG_RESULTS
    printf("value: %s \n", chunk.memory);
#endif //DEBUG_RESULTS

    int nKeys =  getKeysFromJSON(chunk.memory, keys);
    char* cursor = getCursorFromJSON(chunk.memory);
    connection->request = restRequest_setCursor(connection->request, cursor);

    if(queryTable->rows != NULL)
    {
        for(int i = 0; i < queryTable->numberOfRows; i++)
            row_free(queryTable->rows[i]);

        free(queryTable->rows);

    }
    queryTable->rows = malloc(sizeof(Row)*nKeys);
    for(int i = 0; i < nKeys; i++)
    {
        queryTable->rows[i] = row_create(queryTable, keys[i], NULL);
    }
    queryTable->numberOfRows = nKeys;

    free(urlKey);
    free(url);
    free(chunk.memory);
    free(cursor);
    return queryTable;
}

char* getAttributeFromKey(char* key, int index)
{
    if(key == NULL)
        return NULL;
    if(index < 0)
        return NULL;

    int keySize = strlen(key);
    int cont = 0;
    int keyStart = 0;
    int keyEnd = keySize;
    int startFound = 0;

    for(int i = 0; i < keySize; i++)
    {
        if(key[i] == ';')
            cont++;

        if(cont == index + 1)
        {
            keyEnd = i-1;
            break;
        }

        if(cont == index && startFound == 0)
        {
            keyStart = i+1;
            startFound = 1;
        }
    }
    int newStringSize = keyEnd - keyStart+1;
    char* newString = malloc(sizeof(char)*(newStringSize + 1));
    strncpy(newString, key + keyStart, newStringSize);
    newString[newStringSize] = '\0';

    return newString;
}


