#include "sql_create.h"

//#define PRINT_RESULTS_CREATE_TABLE

Results* sql_createTable(DBConnection* connection, char* sql)
{
    Table* table = NULL;
    table = parse_sql_create_table(sql);

    Results* create_results;
    create_results = malloc(sizeof(Results));
    create_results->message = NULL;
    create_results->status = "ERROR";
    create_results->affected_rows = 0;
    create_results->results = NULL;

    if(table == NULL)
    {
        create_results->message = stringConcat(create_results->message, "Table could not be created.");
        freeTable(table);
        return create_results;
    }

    Table* tableFound = findTable(connection->database, table->name);
    if(tableFound != NULL)
    {
        create_results->message = stringConcat(create_results->message, "Table already exists.");

        freeTable(tableFound);
        freeTable(table);
        return create_results;
    }


    addTableToDataBase(connection->database, table);
    freeTable(table);

//#ifdef PRINT_RESULTS_CREATE_TABLE
        printDataBase(connection->database);
//#endif  //PRINT_RESULTS_CREATE_TABLE

    create_results->message = NULL;
    create_results->status = "OK";
    create_results->affected_rows = 0;
    create_results->results = NULL;
    return create_results;
}
