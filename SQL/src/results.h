#ifndef RESULTS_H
#define RESULTS_H

#include <stdlib.h>
#include "dic_dados.h"
#include "csv_parser.h"

/// Create a Row to a specific tableQuery
Row* row_create(QueryTable* queryTable, char* keys, char* values);

Row* row_copy(Row* row);

// Return a new row with only the collunms indicated by indexToSave
Row* row_modify(Row* original, int* indexesToSave, int nIndexes);

void row_free(Row* row);

QueryTable* queryTable_addRow(QueryTable* queryTable, Row* row);

QueryTable* queryTable_addNRows(QueryTable* queryTable, Row** rows, int nRows);

QueryTable* queryTable_copyNRowsToQueryTable(QueryTable* queryTable, Row** rows, int nRows);

QueryTable* queryTable_create(Table* table, Row** rows, int nRows);

QueryTable* queryTable_copy(QueryTable* queryTable);

QueryTable* queryTable_modifyHeader(QueryTable* queryTable, int* indexes, int numberOfIndexes);

QueryTable* queryTable_setHeader(QueryTable* queryTable, Row* newHeader);

void queryTable_print(QueryTable* queryTable);

void results_free(Results* result);

void queryTable_free(QueryTable* queryTable);

#endif // RESULTS_H
