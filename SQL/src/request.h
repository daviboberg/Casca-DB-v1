/* 
 * File:   request.h
 * Author: davib
 *
 * Created on August 24, 2017, 9:37 AM
 */

#ifndef REQUEST_H
#define REQUEST_H

#include "rest.h"
#include <stdlib.h>
#include <string.h>

/// Estrutura de dados para guardar dados de retorno das consultas REST
typedef struct MemoryStruct {
  char *memory;
  size_t size;
}MemoryStruct ;


/// GET Request
/// Return: void
/// Parameters:
///      request:
///          Type: RESTRequest*
///          Descrição: Parameters to make REST Request (url, headers)
///      chunk:
///          Type: struct MemoryStruct*
///          Descrição: return message from REST database
void GET(RESTRequest* connection, struct MemoryStruct* chunk);

/// POST Request
/// Retorno: void
/// Parameters:
///      request:
///          Type: RESTRequest*
///          Description: Parameters to make REST Request (url, headers)
///      data:
///          Type: char*
///          Description: data to POST
///      chunk:
///          Type: struct MemoryStruct*
///          Description:  return message from REST database
void POST(RESTRequest* connection, char* data, struct MemoryStruct* chunk);

/// DELETE Request
/// Retorno: void
/// Parameters:
///      request:
///          Type: RESTRequest*
///          Description: Parameters to make REST Request (url, headers)
///      data:
///          Type: char*
///          Description: data to POST
///      chunk:
///          Type: struct MemoryStruct*
///          Description:  return message from REST database
void DELETE(RESTRequest* connection, struct MemoryStruct* chunk);

/// REST Request
/// Retorno: void
/// Parameters:
///      curl:
///          Type: CURL*
///          Description: CURL connection
///      request:
///          Type: RESTRequest*
///          Description: Parameters to make REST Request (url, headers)
///      data:
///          Type: char*
///          Description: data to POST
///      chunk:
///          Type: struct MemoryStruct*
///          Description:  return message from REST database
char* REST(char* requestType, RESTRequest* connection, char* data, struct MemoryStruct* chunk);

size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

#endif /* REQUEST_H */

