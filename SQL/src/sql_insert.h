#ifndef SQL_INSERT_H
#define SQL_INSERT_H

#include "db.h"
#include "dic_dados.h"
#include "rest.h"

/// Execute query Insert Into
Results* sql_insert(DBConnection* connection, char* sql);

//char* formatKeyAttribute(char* keyAttribute);

char* format_key(char* key);

/// Not complete, do not parse query
/// Execute query Delete From
void sql_delete(DBConnection* connection, char* sql);

#endif // SQL_INSERT_H
