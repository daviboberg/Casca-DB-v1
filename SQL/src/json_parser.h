#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "../lib/cJSON/cJSON.h"
#include <string.h>
#include "dic_dados.h"
#include "config.h"
#include <stdio.h>
#include <stdlib.h>

/// Used to get Keys from JSON received of GET requisition
/// The keys go to Keys[][]
/// Return the number os keys
int getKeysFromJSON(const char* data, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE]);

/// Create and save JSON file called fileName of DataBase metadata
int JSONFromDataBase(DataBase* database, char* fileName);

char* JSONFromQueryTable(QueryTable* queryTable);

char* JSONFromResults(Results* result);

/// Load a JSON file called and convert to DataBase metadata
DataBase* DataBaseFromJSON(char* fileName);

/// From JSON, get itemSize string values from array in object item called itemName
char** getArrayOfStringFromJSON(cJSON* json, char* itemName, int itemSize);

/// From JSON, get itemSize int values from array in object item called itemName
int* getArrayOfIntFromJSON(cJSON* json, char* itemName, int itemSize);

/// Create a JSON with a item called Value and put the values in a array of strings
char* createValuesJSON(char** values, int numberOfValues);

char* getValuesFromJSON(char* key, char* data, int nValues);

char* getCursorFromJSON(char* data);

#endif // JSON_PARSER_H
