#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>
#include "myLib.h"

/// load from file called fileName to string
char* load_schema(char* fileName);

/// Write from string string to file called fileName
void write_schema(char* fileName, const char* string);

/// Write from arrayList(array of strings) to file called fileName
void write_matrix_file(char* fileName, List* arrayList);

/// Write from file called fileName to List (array of strings)
List* load_matrix_file(char* fileName);

/// Append string to end of file called fileName
void append_string_to_file(char* fileName, char* string);

/// Append arrayList (array of strings) to end of file called fileName
void append_matrix_to_file(char* fileName, List* arrayList);




#endif // CONFIG_H
