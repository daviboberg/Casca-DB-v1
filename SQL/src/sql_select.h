#ifndef SQL_SELECT_H
#define SQL_SELECT_H

#include "json_parser.h"
#include "Consulta_Sql.h"
#include "dic_dados.h"
#include "results.h"
#include "relational.h"


/// Execute query Select
Results* sql_select(DBConnection* connection, char* sql);

/// Get keys from Table table of REST database
//int getKeys(DBConnection* connection, char* table, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE]);
QueryTable* getKeys(DBConnection* connection, QueryTable* queryTable, char* table);

QueryTable* getValues(DBConnection* connection, QueryTable* queryTable);

/// Get the index of attribute on key
char* getAttributeFromKey(char* key, int index);




#endif // SQL_SELECT_H
