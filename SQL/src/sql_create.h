#ifndef SQL_CREATE_H
#define SQL_CREATE_H

#include "db.h"

/// Execute query Create Table
Results* sql_createTable(DBConnection* connection, char* sql);

#endif //SQL_CREATE_H
