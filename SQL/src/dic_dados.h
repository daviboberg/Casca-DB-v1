#ifndef DIC_DADOS_H
#define DIC_DADOS_H

#include <stdlib.h>
#include "request.h"
#include "rest.h"
#include "myLib.h"


#define MAX_STRING_SIZE 80
#define MAX_TABLES 5
#define MAX_DATA_CURSOR 50

#define MAX_REST_HEADERS 4




/// Tabela de uma consulta SQL
/// Armazena todos os elementos de uma consulta CREATE TABLE
/// Atributos, Primary keys, foreign keys,
typedef struct Table
{
    char* name;
    char** attributes;
    char** attributeType;
    int numberOfAttributes;
    char** primaryKeys;
    int* indexPrimaryKeys;
    int numberOfPrimaryKeys;
    char** foreignKeys;
    int* indexforeignKeys;
    int numberOfForeignKeys;

}Table;

// Row of a sql select query
typedef struct Row
{
    char** values;
    int length;
}Row;

// Table of a sql select query
// Includes Table definitions and data
typedef struct QueryTable
{
    char* tableName;
    Row* header;
    Row* type;
    int* primaryKeysIndex;
    int numberOfPrimaryKeys;
    Row** rows;
    int numberOfRows;


}QueryTable;

typedef struct Results
{
    int affected_rows;
    char* message;
    char* status;
    QueryTable* results;

}Results;


/// Armazena Metadados de todas as tabelas criadas no banco de dados
typedef struct dataBase
{
    Table** tables;
    int numberOfTables;

}DataBase;

/// Armazena todas as informações necessárias para realizar operações no banco de dados real
/// Metadados das tabelas criadas : DataBase* database
/// Conexão CURL : CURL* curl_connection
/// Informações para consulta REST : RESTRequest* request
typedef struct dbConnection
{
    char* name;
    DataBase* database;
    RESTRequest* request;

}DBConnection;




/// Create a new DataBase
DataBase* create_DataBase();

/// Exclui metadados da tabela table do banco de dados database
// Not implemented
int deleteTableFromDataBase(DataBase* database, Table* table);

/// Adiciona metadados para a Tabela table no banco de dados database
DataBase* addTableToDataBase(DataBase* database, Table* table);

/// Retorna uma nova tabela Table* com os mesmos dados de table
Table* copyTable(Table* table);

/// Verifica se existe a tabela com nome nome e retorna a tabela
Table* findTable(DataBase* database, char* name);

/// Verifica se value é Primary Key da tabela table
int table_checkIsPrimaryKeys(Table* table, char* value);

/// Verifica se value é atributo da tabela table
int table_ckeckIsAttribute(Table* table, char* value);

int table_getAttributeIndex(Table* table, char* attribute);

/// Libera memória de uma tabela Table*
void freeTable(Table* table);

/// Imprime informações de todas as tabelas de um banco de dados db
void printDataBase(DataBase* database);

/// Libera memória de uma banco de dads DataBase*
void freeDataBase(DataBase* database);

// ====== Uso interno





#endif
