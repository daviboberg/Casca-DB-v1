#ifndef CSV_PARSER_H
#define CSV_PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include "myLib.h"

/// Get field num from line of csv
char* getField(char* line, int num);

/// Return a array of string with all the fields on a line of csv
char** getAllFields(char* line);

/// Return the number o fields in a line of csv
int getNumberOfFields(char* line);


List* loadCSVFile(char* fileName, char* tableName);




#endif //CSV_PARSER_H
