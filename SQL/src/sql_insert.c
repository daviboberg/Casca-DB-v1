#include "sql_insert.h"

//#define PRINT_RESULTS_INSERT
//#define PRINT_RESULTS_RETURN

Results* sql_insert(DBConnection* connection, char* sql)
{
    consultaInsert* consulta;
    consulta = parse_sql_insert(sql);
    //consultaInsert_print(*consulta);

    Results* insert_results;
    insert_results = malloc(sizeof(Results));
    insert_results->message = NULL;
    insert_results->status = "ERROR";
    insert_results->affected_rows = 0;
    insert_results->results = NULL;


    

    Table* table = findTable(connection->database, consulta->table);
    if(table == NULL)
    {
        insert_results->message = stringAppend(insert_results->message, "Table ");
        insert_results->message = stringAppend(insert_results->message, consulta->table);
        insert_results->message = stringAppend(insert_results->message, " not found");
        insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;

        consultaInsert_free(consulta);
        freeTable(table);
        return insert_results;

    }

    if(consulta->numberOfValues != (table->numberOfAttributes))
    {
        insert_results->message = stringConcat(insert_results->message, "Invalid number os attributes.");
        insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;

        freeTable(table);
        consultaInsert_free(consulta);

        return insert_results;

    }

    // Create Key and Value
    char* key = stringConcat("", table->name);
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {
        int pkindex = table->indexPrimaryKeys[i];

        key = stringAppend(key, ";");
        key = stringAppend(key, consulta->values[pkindex]);
    }

    char** values = malloc(sizeof(char*)*(consulta->numberOfValues - table->numberOfPrimaryKeys));

    int nValue = 0;
    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        int isPK = 0;
        for(int j = 0; j < table->numberOfPrimaryKeys; j++)
        {
            if(i == table->indexPrimaryKeys[j])
            {
                isPK = 1;
                break;
            }
        }

        if(isPK == 0)
        {
            values[nValue] = consulta->values[i];
            nValue++;
        }
    }


    char* value = createValuesJSON(values, consulta->numberOfValues - table->numberOfPrimaryKeys);
    if(value == NULL)
    {
        printf("Can not create json value.\n");
        free(key);

        for(int i = 0; i < consulta->numberOfValues; i++)
            free(consulta->values[i]);
        free(consulta->values);
        free(consulta->table);
        free(consulta);

        freeTable(table);
        consultaInsert_free(consulta);

        insert_results->message = stringConcat(insert_results->message, "Could not create json value, invalid value.");
        insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;
        return insert_results;
    }

    char* validKey = format_key(key);
    free(key);

#ifdef PRINT_RESULTS_INSERT
        printf("Key: %s\nValue: \n%s\n", validKey, value);
#endif


    char* urlKey = stringConcat(connection->request->url, validKey);
    urlKey = stringAppend(urlKey, "/");

    RESTRequest* newRequest = restRequest_copy(connection->request);

    newRequest = restRequest_setUrl(newRequest, urlKey);

#ifdef PRINT_RESULTS_INSERT
    printf("url: %s\n\n", newRequest->url);
#endif

    MemoryStruct chunk;
    chunk.memory = calloc(1,1);  // will be grown as needed by the realloc above
    chunk.size = 0;    // no data at this point

    POST(newRequest, value, &chunk);

#ifdef PRINT_RESULTS_INSERT
    printf("value: %s \n", chunk.memory);
#endif // PRINT_RESULTS_INSERT


    free(value);
    free(chunk.memory);
    free(urlKey);
    free(validKey);


    freeTable(table);
    consultaInsert_free(consulta);
    // consultaInsert_free desalocas values[i]
    free(values);

    restRequest_free(newRequest);


    insert_results->message = NULL;
    insert_results->status = "OK";
    insert_results->affected_rows = 1;
    insert_results->results = NULL;


    return insert_results;

}

void sql_delete(DBConnection* connection, char* sql)
{
    RESTRequest* newRequest = restRequest_copy(connection->request);

    MemoryStruct chunk;

    chunk.memory = calloc(1,1);  // will be grown as needed by the realloc above
    chunk.size = 0;    // no data at this point

    int strSize = strlen(sql);
    char* semNewLine = malloc(sizeof(char)*strSize);
    strncpy(semNewLine, sql, strSize);
    semNewLine[strSize-1] = '\0';
    char* urlKey = stringConcat(semNewLine, "/");
    char* newURL = stringConcat(connection->request->url, urlKey);

    newRequest = restRequest_setUrl(newRequest, newURL);

    DELETE(newRequest, &chunk);

    //printf("value: %s \n", chunk.memory);

}
