#include "json_parser.h"

int getKeysFromJSON(const char* data, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE])
{

    cJSON* item, *key, *objects;
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    int nKeys = 0;
    int stringSize = 0;

    objects = cJSON_GetObjectItem(json, "objects");

    int arraySize = cJSON_GetArraySize(objects);
    if(arraySize > MAX_DATA_CURSOR)
    {
        printf("Too much select results.\n");
        return -1;
    }

    for(int i = 0; i < arraySize; i++)
    {
        item = cJSON_GetArrayItem(objects, i);
        key = cJSON_GetObjectItem(item, "key");
        stringSize = strlen(key->valuestring) + 1;
        if(stringSize < MAX_STRING_SIZE)
        {
            strncpy(keys[i], key->valuestring, stringSize);
            nKeys++;
        }
    }

    cJSON_Delete(json);
    free(newData);
    return nKeys;
}

int getJSONSize(const char* data)
{
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    int size = cJSON_GetObjectItem(json, "total_num")->valueint;
    cJSON_Delete(json);
    free(newData);
    return size;
}

int JSONFromDataBase(DataBase* database, char* fileName)
{
    cJSON* banco, *table, *item, *array;
    banco = cJSON_CreateArray();

    for(int i = 0; i < database->numberOfTables; i++)
    {

        table = cJSON_CreateObject();

        item = cJSON_CreateString(database->tables[i]->name);
        cJSON_AddItemToObject(table, "Name", item);


        item = cJSON_CreateNumber(database->tables[i]->numberOfAttributes);
        cJSON_AddItemToObject(table, "AttributesSize", item);

        if(database->tables[i]->attributes != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->attributes, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "Attributes", array);
        }

        if(database->tables[i]->attributeType != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->attributeType, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "AttributesType", array);
        }



        item = cJSON_CreateNumber(database->tables[i]->numberOfPrimaryKeys);
        cJSON_AddItemToObject(table, "PrimaryKeysSize", item);

        if(database->tables[i]->primaryKeys != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->primaryKeys, database->tables[i]->numberOfPrimaryKeys);
            cJSON_AddItemToObject(table, "PrimaryKeys", array);

            array = cJSON_CreateIntArray((const int*)database->tables[i]->indexPrimaryKeys, database->tables[i]->numberOfPrimaryKeys);
            cJSON_AddItemToObject(table, "IndexPrimaryKeys", array);
        }



        item = cJSON_CreateNumber(database->tables[i]->numberOfForeignKeys);
        cJSON_AddItemToObject(table, "ForeignKeysSize", item);

        if(database->tables[i]->foreignKeys != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->foreignKeys, database->tables[i]->numberOfForeignKeys);
            cJSON_AddItemToObject(table, "ForeignKeys", array);

            array = cJSON_CreateIntArray((const int*)database->tables[i]->indexforeignKeys, database->tables[i]->numberOfForeignKeys);
            cJSON_AddItemToObject(table, "IndexForeignKeys", array);
        }

        cJSON_AddItemToArray(banco, table);
    }


    char* out = cJSON_Print(banco);
    write_schema(fileName, out);

    //free(out);
    //cJSON_Delete(banco);

    return 1;
}

char* JSONFromQueryTable(QueryTable* queryTable)
{

/*
    {
   "rows" : 1               // INT - number of affected/returned rows
   "status" : "exemplo"      // String (OK/ERROR)
   "message" : "msg"        // String - error message
   "results" :  {
                "Name":	"name",
                "Header":	[itens],
                "NumberOfRows":	1,
                "Rows":	{
                            "0":	[values],
                            "1":    [values2]
                        }
                }
    }
*/


    cJSON *table, *item, *array, *rows;
    table = cJSON_CreateArray();



    table = cJSON_CreateObject();

    item = cJSON_CreateString(queryTable->tableName);
    cJSON_AddItemToObject(table, "Name", item);


    if(queryTable->header!= NULL)
    {
        array = cJSON_CreateStringArray((const char**)queryTable->header->values, queryTable->header->length);
        cJSON_AddItemToObject(table, "Header", array);
    }

    item = cJSON_CreateNumber(queryTable->numberOfRows);
    cJSON_AddItemToObject(table, "NumberOfRows", item);

    rows = cJSON_CreateObject();

    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        if(queryTable->rows[i] != NULL)
        {
            array = cJSON_CreateStringArray((const char**)queryTable->rows[i]->values, queryTable->rows[i]->length);
            char str[7];
            sprintf(str, "%d", i);
            cJSON_AddItemToObject(rows, str, array);
        }

    }

    cJSON_AddItemToObject(table, "Rows", rows);





    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);

    return out ;
}

char* JSONFromResults(Results* result)
{
    cJSON *table, *item, *array, *rows, *results;

    table = cJSON_CreateObject();


    item = cJSON_CreateNumber(result->affected_rows);
    cJSON_AddItemToObject(table, "Rows", item);

    item = cJSON_CreateString(result->status);
    cJSON_AddItemToObject(table, "Status", item);

    if(result->message != NULL)
    {
        item = cJSON_CreateString(result->message);

    }
    else
    {
        item = cJSON_CreateString("");
    }
    cJSON_AddItemToObject(table, "Message", item);


    results = cJSON_CreateObject();

    if(result->results != NULL)
    {


        item = cJSON_CreateString(result->results->tableName);
        cJSON_AddItemToObject(results, "Name", item);


        if(result->results->header!= NULL)
        {
            array = cJSON_CreateStringArray((const char**)result->results->header->values, result->results->header->length);
            cJSON_AddItemToObject(results, "Header", array);
        }

        item = cJSON_CreateNumber(result->results->numberOfRows);
        cJSON_AddItemToObject(results, "NumberOfRows", item);

        rows = cJSON_CreateObject();

        for(int i = 0; i < result->results->numberOfRows; i++)
        {
            if(result->results->rows[i] != NULL)
            {
                array = cJSON_CreateStringArray((const char**)result->results->rows[i]->values, result->results->rows[i]->length);
                char str[7];
                sprintf(str, "%d", i);
                cJSON_AddItemToObject(rows, str, array);
            }

        }

        cJSON_AddItemToObject(results, "Rows", rows);
    }

    cJSON_AddItemToObject(table, "Results", results);




    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);

    return out ;
}

// Mudar para forma mais genercia que use a mesma função para selects e inserts
char* JSON_insert_result(char* message)
{

/*
    {
   "rows" : 1               // INT - number of affected/returned rows
   "status" : "exemplo"      // String (OK/ERROR)
   "message" : "msg"        // String - error message
   "results" :  {}
    }
*/


    cJSON *table, *item, *results;
    table = cJSON_CreateArray();



    table = cJSON_CreateObject();


    item = cJSON_CreateNumber(1);
    cJSON_AddItemToObject(table, "Rows", item);

    item = cJSON_CreateString("OK");
    cJSON_AddItemToObject(table, "Status", item);


    item = cJSON_CreateString(message);
    cJSON_AddItemToObject(table, "Message", item);


    results = cJSON_CreateObject();


    //cJSON_AddItemToObject(results, "Rows", rows);

    cJSON_AddItemToObject(table, "Results", results);




    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);

    return out ;
}


DataBase* DataBaseFromJSON(char* fileName)
{

    char* loaded = load_schema(fileName);

    if(loaded == NULL)
    {
        return NULL;
    }

    cJSON* json = cJSON_Parse(loaded);
    cJSON* item, *key;
    DataBase* database = calloc(1, sizeof(DataBase));

    int stringSize = 0;

    if(cJSON_IsArray(json) == 1)
    {
        int arraySize = cJSON_GetArraySize(json);


        database->numberOfTables = arraySize;
        database->tables = calloc(arraySize, sizeof(Table*));

        for(int i = 0; i < database->numberOfTables; i++)
        {
            database->tables[i] = calloc(1, sizeof(Table));

            item = cJSON_GetArrayItem(json, i);
            key = cJSON_GetObjectItem(item, "Name");
            if(key == NULL)
            {
                printf("Key Name not found\n");
            }
            else
            {
                stringSize = strlen(key->valuestring);
                database->tables[i]->name = malloc(sizeof(char)*(stringSize+1));
                strncpy(database->tables[i]->name, key->valuestring, stringSize);
                database->tables[i]->name[stringSize] = '\0';
            }

            key = cJSON_GetObjectItem(item, "AttributesSize");
            if(key == NULL)
            {
                printf("Key AttributesSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfAttributes = key->valueint;
                database->tables[i]->attributes = getArrayOfStringFromJSON(item, "Attributes", database->tables[i]->numberOfAttributes);
            }

            key = cJSON_GetObjectItem(item, "AttributesSize");
            if(key == NULL)
            {
                printf("Key AttributesSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfAttributes = key->valueint;
                database->tables[i]->attributeType = getArrayOfStringFromJSON(item, "AttributesType", database->tables[i]->numberOfAttributes);
            }


            key = cJSON_GetObjectItem(item, "PrimaryKeysSize");
            if(key == NULL)
            {
                printf("Key PrimaryKeysSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfPrimaryKeys = key->valueint;
                database->tables[i]->primaryKeys = getArrayOfStringFromJSON(item, "PrimaryKeys", database->tables[i]->numberOfPrimaryKeys);
                database->tables[i]->indexPrimaryKeys = getArrayOfIntFromJSON(item, "IndexPrimaryKeys", database->tables[i]->numberOfPrimaryKeys);
            }


            key = cJSON_GetObjectItem(item, "ForeignKeysSize");
            if(key == NULL)
            {
                printf("Key ForeignKeysSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfForeignKeys = key->valueint;
                database->tables[i]->foreignKeys = getArrayOfStringFromJSON(item, "ForeignKeys", database->tables[i]->numberOfForeignKeys);
                database->tables[i]->indexforeignKeys = getArrayOfIntFromJSON(item, "IndexForeignKeys", database->tables[i]->numberOfForeignKeys);
            }

        }
    }

    cJSON_Delete(json);
    free(loaded);
    return database;

}

char** getArrayOfStringFromJSON(cJSON* json, char* itemName, int itemSize)
{
    cJSON* item, *key;
    char** newMatrix = NULL;

    if(json == NULL)
    {
        printf("Invalid json\n");
        return NULL;
    }

    if(itemName == NULL)
    {
        printf("Invalid item\n");
        return NULL;
    }

    if(itemSize == 0)
        return NULL;


    item = cJSON_GetObjectItem(json, itemName);

    int stringSize;
    newMatrix = calloc(itemSize+1, sizeof(char*));
    for(int i = 0; i < itemSize; i++)
    {
        key = cJSON_GetArrayItem(item, i);


        if(key == NULL)
        {
            printf("Item not found.\n");
            continue;
        }

        stringSize = strlen(key->valuestring) + 1;
        if(stringSize < MAX_STRING_SIZE)
        {
            newMatrix[i] = malloc(sizeof(char)*stringSize);
            strncpy(newMatrix[i], key->valuestring, stringSize);
        }
    }

    //cJSON_Delete(item);
    return newMatrix;

}

int* getArrayOfIntFromJSON(cJSON* json, char* itemName, int itemSize)
{
    cJSON* item, *key;
    int* newMatrix = NULL;

    if(json == NULL)
    {
        printf("Invalid json\n");
        return NULL;
    }

    if(itemName == NULL)
    {
        printf("Invalid item\n");
        return NULL;
    }

    if(itemSize == 0)
        return NULL;


    item = cJSON_GetObjectItem(json, itemName);

    newMatrix = calloc(itemSize+1, sizeof(int));
    for(int i = 0; i < itemSize; i++)
    {
        key = cJSON_GetArrayItem(item, i);


        if(key == NULL)
        {
            printf("Item not found.\n");
            continue;
        }

        newMatrix[i] = key->valueint;

    }

    //cJSON_Delete(item);
    return newMatrix;
}

char* createValuesJSON(char** values, int numberOfValues)
{

    cJSON *array, *table;

    if(values == NULL)
    {
        printf("Invalid values.\n");
        return NULL;
    }
    if(numberOfValues < 0)
    {
        printf("Invalid numberOfValues.\n");
        return NULL;
    }

    table = cJSON_CreateObject();
    array = cJSON_CreateStringArray((const char**)values, numberOfValues);
    cJSON_AddItemToObject(table, "Value", array);

    char* value = cJSON_Print(table);
    cJSON_Delete(table);
    return value;
}

char* getValuesFromJSON(char* key, char* data, int nValues)
{
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    char** values = getArrayOfStringFromJSON(json, "value", nValues);

    char* value = NULL;

    if(nValues == 0)
        return NULL;

    value = stringAppend(value, values[0]);
    free(values[0]);
    for(int i = 1; i < nValues; i++)
    {
        value = stringAppend(value, ";");
        value = stringAppend(value, values[i]);
        free(values[i]);
    }

    free(values);
    cJSON_Delete(json);
    free(newData);

    return value;
}

char* getCursorFromJSON(char* data)
{
    cJSON* key;
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);


    int stringSize = 0;
    char* cursor = NULL;

    if(cJSON_IsObject(json) == 1)
    {
        key = cJSON_GetObjectItem(json, "has_more");
        if(key->valueint == 1)
        {
            key = cJSON_GetObjectItem(json, "cursor");
            stringSize = strlen(key->valuestring);
            if(stringSize < MAX_STRING_SIZE)
            {
                cursor = malloc(sizeof(char)*(stringSize + 1));
                strncpy(cursor, key->valuestring, stringSize);
                cursor[stringSize] = '\0';
            }
        }
    }
    cJSON_Delete(json);
    free(newData);

    return cursor;
}
