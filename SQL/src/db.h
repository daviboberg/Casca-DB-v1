#ifndef DB_H
#define DB_H

#include "dic_dados.h"
#include "sql_query.h"
#include "json_parser.h"



/// Create a DBConnection
/// Description: Load database metadata if exists, connect CURL, set Rest_request and others
/// Parameters:
///     localDataBase:
///         type: char*
///         Description: Name of the file that keep the database metadata
///     rest:
///         type: RESTRequest*
///         Description: Parameter to communicate with rest databases
DBConnection* db_connectRest(char* localDataBase, RESTRequest* rest);

/// Disconnect a DBConnection
/// Description: Save database metadata to file, disconnect CURL and free parameters
/// Description: Load database metadata if exists and connect CURL
/// Parameters:
///     rest
///         type: RESTRequest*
///         Description: Parameter to communicate with rest databases
void db_disconnect(DBConnection* database);

/// Load a new RESTRequest to DBConnection
DBConnection* db_setRestRequest(DBConnection* connection, RESTRequest* newRequest);

/// Load database metadata if exists
DataBase* retrieveDataBase(char* localDataBase, RESTRequest* request);



#endif /* DB_H */

