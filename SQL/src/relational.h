
#ifndef RELATIONAL_H
#define RELATIONAL_H


#include "results.h"
#include "Consulta_Sql.h"

// Return a queryTable modified, with only the selected columns
QueryTable* projection(QueryTable* queryTable, char* attributes[MAX_ATTRIBUTES_MATCH], int numberOfAttributes);


// Same as projection, but the columns to keep are defined by columnsToSave
//QueryTable* projectionByIndex(QueryTable* queryTable, int* columnsToSave);

// Return a queryTable modified, with only the selected rows
QueryTable* selection(QueryTable* queryTable, char* conditions[MAX_CONDITIONS_MATCH][3], int numberOfConditions);


int operation(char *conditionValue, char *key, char* sqlOperation, char* type);


#endif /* RELATIONAL_H */

