#include "csv_parser.h"

char** getAllFields(char* line)
{
    if(line == NULL)
    {
        return NULL;
    }
    int num = getNumberOfFields(line);
    char** fields = malloc(sizeof(char*)*num);
    char* tmp = copyString(line);

    int stringSize;
    const char* tok;
    tok = strtok(tmp, ";");
    for(int i = 0; i < num; i++)
    {
        stringSize = strlen(tok);
        fields[i] = malloc(sizeof(char)*(stringSize+1));
        strncpy(fields[i], tok, stringSize);
        fields[i][stringSize] = '\0';
        tok = strtok(NULL,";\n");
    }
    free(tok);
    free(tmp);
    return fields;
}

char* getField(char* line, int num)
{
    char* tok;
    int count = 0;
    char* tmp = strdup(line);
    for (tok = strtok(tmp, ";,"); tok != NULL; tok = strtok(NULL, ";\n"))
    {
        count++;
        if (count == num)
        {
            tok = strdup(tok);
            free(tmp);
            return tok;
        }
    }
    free(tmp);
    return NULL;
}

int getNumberOfFields(char* line)
{
    if(line == NULL)
        return 0;

    const char* tok;
    int count = 0;
    char* tmp = strdup(line);
    // For( primeiro token, enquanto token é o primeiro elemento, até
    for(tok = strtok(tmp,";"); tok != NULL; tok = strtok(NULL, ";\n"))
    {
        count++;
    }
    free(tmp);
    return count;
}


List* loadCSVFile(char* fileName, char* tableName)
{
    FILE* stream = fopen(fileName, "r");

    int cont;
    int contHeaders = 0;
    int lines = 0;
    char** elements;
    char* nome = NULL;
    char buffer[1024];
    int stringSize;

    List* fields = NULL;


    int contN = 0;
    while (fgets(buffer, sizeof(buffer), stream))
    {
        contN++;
        cont = getNumberOfFields(buffer);
        if(cont == 32)
        {
            if(contHeaders < 2)
            {
                contHeaders++;
                continue;
            }


            elements = getAllFields(buffer);
            char* insert = NULL;
            for(int i = 0; i < 32; i++)
            {
                insert = stringAppend(insert, ",");
                insert = stringAppend(insert, elements[i]);
                free(elements[i]);
            }
            fields = listAppend(fields, insert);
            lines++;
            free(insert);
            free(elements);

        }
        char* value = getField(buffer, 1);
        if(strncmp(value, "\"Cliente (nome)\"", strlen("\"Cliente (nome)\"")) == 0)
        {
            free(value);
            value = getField(buffer, 2);
            if(value != NULL)
            {
                stringSize = strlen(value);
                nome = calloc(stringSize + 1, sizeof(char));
                strncpy(nome, value, stringSize);
                nome[stringSize] = '\0';
                free(value);
            }

        }
        else if(value != NULL)
            free(value);
    }

    char* newNome = eliminateDelimiter(nome, '\"');
    free(nome);

    char *insertInto;
    insertInto = stringConcat("Insert into ", tableName);
    insertInto = stringAppend(insertInto, " (");
    insertInto = stringAppend(insertInto, "'");
    insertInto = stringAppend(insertInto, newNome);
    insertInto = stringAppend(insertInto, "'");

    List* listInsert = NULL;
    char* insert;
    for(int i = 0; i < lines; i++)
    {

        insert = stringConcat(insertInto, fields->values[i]);
        insert = stringAppend(insert, ")");

        listInsert = listAppend(listInsert, insert);
        free(insert);
    }

    free(newNome);
    for(int i = 0; i < fields->listSize; i++)
        free(fields->values[i]);
    free(fields->values);
    free(fields);

    return listInsert;

}


