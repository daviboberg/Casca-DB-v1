#include "db.h"

DBConnection* db_connectRest(char* localDataBase, RESTRequest* request)
{
    DBConnection* connection = malloc(sizeof(DBConnection));
    connection->database = retrieveDataBase(localDataBase, request);
    connection->request = NULL;
    connection = db_setRestRequest(connection, request);

    int stringLength = strlen(localDataBase);
    connection->name = malloc(sizeof(char)*(stringLength+1));
    strncpy(connection->name, localDataBase, stringLength);
    connection->name[stringLength] = '\0';

    return connection;
}

void db_disconnect(DBConnection* connection)
{

    JSONFromDataBase(connection->database, connection->name);
    freeDataBase(connection->database);
    free(connection->name);
    restRequest_free(connection->request);
    free(connection);
}

DataBase* retrieveDataBase(char* localDataBase, RESTRequest* request)
{
    DataBase* newDataBase = DataBaseFromJSON(localDataBase);
    if(newDataBase == NULL)
    {
        newDataBase = create_DataBase();
    }

    return newDataBase;
}

DBConnection* db_setRestRequest(DBConnection* connection, RESTRequest* newRequest)
{
    if(connection == NULL)
    {
        printf("DBConnection Null.\n");
        return NULL;
    }
    if(newRequest == NULL)
    {
        printf("New Request is null. Nothing done.\n");
        return connection;
    }
    if(connection->request != NULL)
        restRequest_free(connection->request);

    connection->request = newRequest;
    return connection;
}
