#ifndef REST_H
#define REST_H


#include <stdio.h>
#include <stdlib.h>


typedef struct REST_Request{

    char** header;
    unsigned int numberOfHeaders;
    char* url;
    char* cursor;

}RESTRequest;

// Retorna um Rest_Reques*t com informações identicas
RESTRequest* restRequest_init();

RESTRequest* restRequest_copy(RESTRequest* request);

/// Define a URL da requisição rest
RESTRequest* restRequest_setUrl(RESTRequest *request, char* to_append);

RESTRequest* restRequest_setCursor(RESTRequest *request, char* newCursor);

RESTRequest* restRequest_setHeader(RESTRequest* request, int index, char* toSet);

/// Adiciona um header a requisição rest
RESTRequest* restRequest_appendHeader(RESTRequest *request, char* to_append);

/// Libera memória do RESTRequest
void restRequest_free(RESTRequest *request);

/// Libera memória dos headers do RESTRequest
void restRequest_freeHeaders(RESTRequest *request);

/// Libera memória da url do RESTRequest
void restRequest_freeUrl(RESTRequest *request);

void restRequest_freeCursor(RESTRequest *request);

size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

#endif	// REST_H

