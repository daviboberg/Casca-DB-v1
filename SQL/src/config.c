#include "config.h"

extern pthread_mutex_t dbfile_mutex;
char* load_schema(char* fileName)
{   
    pthread_mutex_lock(&dbfile_mutex);
    FILE *f = fopen(fileName, "rb");
    if(f == NULL)
    {
        printf("\nDataBase not found.\n");
        return NULL;
    }
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    char *string = malloc(fsize + 1);
    int size = fread(string, fsize, 1, f);
        
    fclose(f);

    if(size == 0)
        return string;
    string[fsize] = 0;
    pthread_mutex_unlock(&dbfile_mutex);
    return string;
}

void write_schema(char* fileName, const char* string)
{
    pthread_mutex_lock(&dbfile_mutex);
    if(fileName == NULL)
    {
        printf("\nwrite_schema failed, fileName not allocated\n");
        return;
    }
    if(string == NULL)
    {
        printf("\nwrite_schema failed, data to save not allocated\n");
        return;
    }

    FILE *f = fopen(fileName, "w");
    if(f == NULL)
    {
        printf("\nwrite_schema failed, can not open file %s\n", fileName);
        return;
    }

    fprintf(f, string);

    fclose(f);
    pthread_mutex_unlock(&dbfile_mutex);
}

void write_matrix_file(char* fileName, List* arrayList)
{
    pthread_mutex_lock(&dbfile_mutex);
    if(fileName == NULL)
    {
        printf("\nwrite_schema failed, fileName not allocated\n");
        return;
    }
    if(arrayList->values == NULL)
    {
        printf("\nwrite_schema failed, data to save not allocated\n");
        return;
    }

    FILE *f = fopen(fileName, "w");
    if(f == NULL)
    {
        printf("\nwrite_schema failed, can not open file %s\n", fileName);
        return;
    }

    for(int i = 0; i < arrayList->listSize; i++)
    {
        fprintf(f, arrayList->values[i]);
        if(i < arrayList->listSize-1)
            fprintf(f, "\n");
    }

    fclose(f);
    pthread_mutex_unlock(&dbfile_mutex);
}

List* load_matrix_file(char* fileName)
{
    pthread_mutex_lock(&dbfile_mutex);
    FILE *f = fopen(fileName, "rb");
    if(f == NULL)
    {
        printf("\nDataBase not found.\n");
        return NULL;
    }

    char buffer[1024];
    List* list = NULL;

    while (fgets(buffer, sizeof(buffer), f))
    {
        list = listAppend(list, buffer);
    }
    pthread_mutex_unlock(&dbfile_mutex);
    return list;
}

void append_string_to_file(char* fileName, char* string)
{
    pthread_mutex_lock(&dbfile_mutex);
    if(fileName == NULL)
    {
        printf("\nwrite_schema failed, fileName not allocated\n");
        return;
    }
    if(string == NULL)
    {
        printf("\nwrite_schema failed, data to save not allocated\n");
        return;
    }

    FILE *f = fopen(fileName, "a");
    if(f == NULL)
    {
        printf("\nwrite_schema failed, can not open file %s\n", fileName);
        return;
    }

    fprintf(f, string);
    fprintf(f, "\n");

    fclose(f);
    pthread_mutex_unlock(&dbfile_mutex);
}

void append_matrix_to_file(char* fileName, List* arrayList)
{
    pthread_mutex_lock(&dbfile_mutex);
    if(fileName == NULL)
    {
        printf("\nwrite_schema failed, fileName not allocated\n");
        return;
    }
    if(arrayList->values == NULL)
    {
        printf("\nwrite_schema failed, data to save not allocated\n");
        return;
    }

    FILE *f = fopen(fileName, "a");
    if(f == NULL)
    {
        printf("\nwrite_schema failed, can not open file %s\n", fileName);
        return;
    }

    for(int i = 0; i < arrayList->listSize; i++)
    {
        fprintf(f, arrayList->values[i]);
        if(i < arrayList->listSize - 1)
            fprintf(f, "\n");
    }

    fclose(f);
    pthread_mutex_unlock(&dbfile_mutex);
}
