#include "db.h"

// Para testar
#include "csv_parser.h"
#include <string.h>

// Para benchmark
#include <time.h>
#include <math.h>

//#define BENCHMARK

int main(int argc, char** argv)
{

    DBConnection* connection;

    RESTRequest* request = restRequest_init();


    char* url = argv[1];
    request = restRequest_setUrl(request, url);
    request = restRequest_appendHeader(request, argv[2]);
    request = restRequest_appendHeader(request, argv[3]);
    connection = db_connectRest(argv[4], request);

/*
    char* url = "https://sgx1.chocolate-cloud.cc/kvs/v3/datastores/consumption/";
    request = restRequest_setUrl(request, url);
    request = restRequest_appendHeader(request, "Authorization: ApiKey 0131Byd7N220T32qp088kIT53ryT113i");
    request = restRequest_appendHeader(request, "Content-Type: application/json");

    connection = db_connectRest("../database.db", request);
*/



    query(connection, argv[5]);


    db_disconnect(connection);
    //restRequest_free(request);


    return 0;
}
