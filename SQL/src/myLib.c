#include "myLib.h"

char* eliminateDelimiter(char* string, const char delimiter)
{
    int startDelimFound = 0;
    int strSize = strlen(string);
    int start = 0;
    int end = 0;
    int match = 0;

    int i = 0;
    while(i < strSize)
    {
        if(string[i] == delimiter )
        {
            if(startDelimFound == 0)
            {
                start = i;
                startDelimFound = 1;
            }
            else if(startDelimFound == 1)
            {
                end = i;
                match = 1;
                break;
            }
        }
        i++;
    }

    if(match == 0)
        return NULL;

    int length = end - start;
    char* newString = malloc(sizeof(char)*length);
    strncpy(newString, string + sizeof(char)*(start + 1), length);
    newString[length-1] = '\0';
    return newString;
}

char* eliminateCharacter(char* string, const char character)
{
    int strSize = strlen(string);
    int match = 0;


    int i = 0;
    for(int i = 0; i < strSize; i++)
    {
        if(string[i] == character)
            match++;

    }

    if(match == 0)
        return string;

    int index = 0;
    int length = strSize - match + 1;
    char* newString = malloc(sizeof(char)*length);
    for(int i = 0; i < strSize; i++)
    {
        if(string[i] == character)
            continue;
        else
        {
            newString[index] = string[i];
            index++;
        }
    }
    newString[index] = '\0';
    return newString;

}

char* stringConcat(char* string1, char* string2)
{
    int lengthString1, lengthString2;
    if(string1 == NULL)
        lengthString1 = 0;
    else
        lengthString1 = strlen(string1);
    if(string2 == NULL)
    {
        printf("String to append not allocated\n");
        return NULL;
    }
    lengthString2 = strlen(string2);

    char* newString = malloc(sizeof(char)*(lengthString1 + lengthString2 + 1));

    if(string1 != NULL)
        memcpy(newString, string1, lengthString1);
    memcpy(newString + lengthString1, string2, lengthString2);
    newString[lengthString1+lengthString2] = '\0';

    return newString;
}

char* stringAppend(char* string1, char* string2)
{
    int lengthString1, lengthString2;
    if(string1 == NULL)
        lengthString1 = 0;
    else
        lengthString1 = strlen(string1);

    if(string2 == NULL)
    {
        printf("String to append not allocated\n");
        return NULL;
    }
    lengthString2 = strlen(string2);

    if(string1 != NULL)
    {
        char* newString = realloc(string1, sizeof(char)*(lengthString1+lengthString2+1));
        if(newString == NULL)
        {
            printf("Failed to realloc string.\n");
            return string1;
        }
        string1 = newString;
    }
    else
    {
        string1 = malloc(sizeof(char)*(lengthString2+1));
    }

    memcpy(string1 + lengthString1, string2, lengthString2);
    string1[lengthString1+lengthString2] = '\0';

    return string1;

}

char* copyString(char* string)
{
    char *newString = NULL;

    if(string != NULL)
    {
        int size = strlen(string);
        newString = malloc(sizeof(char)*(size+1));
        newString = strncpy(newString, string, size);
        newString[size] = '\0';
    }
    else
    {
        printf("\nCopyString failed, string NULL");
    }

    return newString;
}

char** copyArrayOfString(char** array, int n)
{

    char** newArray = NULL;
    if(n < 0)
    {
        printf("Invalid number of string.\n");
    }
    if(array == NULL)
    {
        //printf("Invalid array.\n");
        return NULL;
    }

    newArray = malloc(sizeof(char*)*n);
    for(int i = 0; i < n; i++)
    {
        newArray[i] = copyString(array[i]);
    }

    return newArray;
}

int* copyArrayOfInt(int* array, int n)
{
    int *newArray = NULL;

    if(n < 0)
    {
        printf("\ncopyMatrix failed, invalid n\n");
        return NULL;
    }

    if(array != NULL)
    {
        newArray = malloc(sizeof(int)*n);
        for(int i = 0; i < n; i++)
        {
            newArray[i] = array[i];
        }
    }
    else
    {
        return NULL;
    }

    return newArray;
}

List* copyList(List* list)
{
    if(list == NULL)
    {
        printf("Invalid list.\n");
        return NULL;
    }

    List* newList = NULL;

    newList->values = copyArrayOfString(list->values, list->listSize);
    newList->listSize = list->listSize;

    return newList;
}

List* listAppend(List* list, char* string)
{
    List* newList = NULL;

    if(string == NULL)
    {
        printf("String to append not allocated.\n");
        return list;
    }

    if(list == NULL)
    {
        newList = calloc(1, sizeof(List));
        newList->values = calloc(1, sizeof(char*));
        newList->listSize = 0;
    }
    else
    {
        char** values = NULL;
        if(list->values == NULL)
        {
            values = calloc(1, sizeof(char*));
            list->listSize = 0;
        }
        else
            values = realloc(list->values, sizeof(char*)*(list->listSize + 1));

        if(values == NULL)
        {
        //out of memory!
            printf("\nFailed to append\n");
            return list;
        }
        newList = calloc(1, sizeof(List));
        newList->values = values;
        newList->listSize = list->listSize;
        free(list);
    }


    list = newList;
    int stringSize = strlen(string);
    list->values[list->listSize] = calloc(stringSize + 1, sizeof(char));
    strncpy(list->values[list->listSize], string, stringSize);
    list->values[list->listSize][stringSize] = '\0';
    list->listSize++;

    return list;

}

void freeList(List* list)
{
    if(list == NULL)
        return;
    for(int i = 0; i < list->listSize; i++)
    {
        if(list->values[i] == NULL)
            continue;
        free(list->values[i]);
    }
    free(list->values);
    free(list);
}


// Replace invalid caracters
char* format_key(char* key)
{
    char* validKey = NULL;
    int pos_init = 0;
    int pos_end = 0;
    
    for(int i = 0; i < strlen(key); i++)
    {
        if(key[i] == ' ')
        {
            char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
            substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
            substring[pos_end - pos_init + 1] = '\0';
            validKey = stringAppend(validKey, substring);
            validKey = stringAppend(validKey, "%20");
            pos_init = i+1;
            free(substring);
        }
        else if(key[i] == '/')
        {
            char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
            substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
            substring[pos_end - pos_init + 1] = '\0';
            validKey = stringAppend(validKey, substring);
            validKey = stringAppend(validKey, "%2F");
            pos_init = i + 1;
            free(substring);

        }
        else
        {
            pos_end = i;
        }
    }
    if(pos_end != pos_init)
    {
        char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
        substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
        substring[pos_end - pos_init + 1] = '\0';
        validKey = stringAppend(validKey, substring);
        free(substring);
    }

    return validKey;

}
