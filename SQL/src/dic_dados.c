#include "dic_dados.h"

DataBase* create_DataBase()
{
    DataBase* newDB = calloc(1, sizeof(DataBase));
    newDB->numberOfTables = 0;
    newDB->tables = NULL;

    return newDB;
}

DataBase* addTableToDataBase(DataBase* database, Table* table)
{
    int newSize;
    int size;



    newSize = database->numberOfTables + 1;
    size = database->numberOfTables;
    Table** tables;
    if(database->tables == NULL || size == 0)
    {
        tables = malloc(sizeof(Table*));
    }
    else
        tables = (Table**)realloc(database->tables, sizeof(Table*)*newSize);

    if(tables == NULL) {
        //out of memory!
        printf("\nFailed to realloc, Table no add to DataBase\n");
        return NULL;
    }

    database->tables = tables;
    database->tables[size] = copyTable(table);
    database->numberOfTables = newSize;

    return database;
}


Table* copyTable(Table* table)
{
    Table* newTable = malloc(sizeof(Table));

    if(table == NULL)
        return NULL;

    newTable->name = copyString(table->name);

    newTable->attributes = copyArrayOfString(table->attributes, table->numberOfAttributes);
    newTable->attributeType = copyArrayOfString(table->attributeType, table->numberOfAttributes);

    newTable->numberOfAttributes = table->numberOfAttributes;



    newTable->foreignKeys = copyArrayOfString(table->foreignKeys, table->numberOfForeignKeys);
    newTable->indexforeignKeys = copyArrayOfInt(table->indexforeignKeys, table->numberOfForeignKeys);

    newTable->numberOfForeignKeys = table->numberOfForeignKeys;



    newTable->primaryKeys = copyArrayOfString(table->primaryKeys, table->numberOfPrimaryKeys);
    newTable->indexPrimaryKeys = copyArrayOfInt(table->indexPrimaryKeys, table->numberOfPrimaryKeys);

    newTable->numberOfPrimaryKeys = table->numberOfPrimaryKeys;



    return newTable;
}

int dropTable(DataBase* db, Table* table)
{

    if(db == NULL)
    {
        return -1;
    }
    else if(db->numberOfTables == 0)
    {
        return 0;
    }


    int posTable = 0;
    char found = 0;
    for(int i = 0; i < db->numberOfTables; i++)
    {
        if(db->tables[i] == table)
        {
            posTable = i;
            found = 1;
            break;
        }
    }

    if(found == 0)
    {
        printf("Tabela não encontrada!/n");
        return -1;
    }

    freeTable(db->tables[posTable]);
    db->tables[posTable] = db->tables[db->numberOfTables-1];
    db->numberOfTables--;
    db->tables = realloc(db->tables, sizeof(Table*)*db->numberOfTables);

    if(db->tables == NULL) {
        //out of memory!
        printf("Can't allocate memory (realloc returned NULL)\n");
        return -1;
    }

    return 0;
}

Table* findTable(DataBase* database, char* name)
{
    

    if(database == NULL)
    {
        printf("database null\n");
        return NULL;
    }
    if(database->numberOfTables == 0)
    {
        printf("no tables\n");
        return NULL;
    }

    for(int i = 0; i < database->numberOfTables; i++)
    {
        
        if(strcmp(database->tables[i]->name, name) == 0)
        {
            
            return copyTable(database->tables[i]);
        }

    }
    return NULL;

}

int table_checkIsPrimaryKeys(Table* table, char* value)
{
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {

        if(strncmp(value, table->primaryKeys[i], strlen(table->primaryKeys[i])) == 0)
            return 1;
    }

    return 0;
}

int table_ckeckIsAttribute(Table* table, char* value)
{
    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        if(strncmp(value, table->attributes[i], strlen(table->attributes[i])) == 0)
            return 1;

    }
    return 0;
}

int table_getAttributeIndex(Table* table, char* attribute)
{
    if(attribute == NULL || table == NULL || table->attributes == NULL)
        return -1;

    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        if(strncmp(table->attributes[i], attribute, strlen(table->attributes[i]))==0)
            return i;
    }
    return -1;
}

void freeTable(Table* table)
{
    if(table == NULL)
        return;

    if(table->name != NULL)
    {
        free(table->name);
    }

    if(table->attributes != NULL)
    {
        for(int i = 0; i < table->numberOfAttributes; i++)
        {
            if(table->attributes[i] == NULL)
                continue;
            free(table->attributes[i]);
            table->attributes[i] = NULL;
        }
        free(table->attributes);
        table->attributes = NULL;
    }

    if(table->attributeType!= NULL)
    {
        for(int i = 0; i < table->numberOfAttributes; i++)
        {
            free(table->attributeType[i]);
            table->attributeType[i] = NULL;
        }
        free(table->attributeType);
        table->attributeType = NULL;
        table->numberOfAttributes = 0;
    }


    if(table->numberOfForeignKeys > 0)
    {
        for(int i = 0; i < table->numberOfForeignKeys; i++)
        {
            if(table->foreignKeys[i] == NULL)
                continue;
            free(table->foreignKeys[i]);
            table->foreignKeys[i] = NULL;
        }
        free(table->foreignKeys);
        free(table->indexforeignKeys);
        table->foreignKeys = NULL;
        table->numberOfForeignKeys = 0;
    }


    if(table->numberOfPrimaryKeys > 0)
    {
        for(int i = 0; i < table->numberOfPrimaryKeys; i++)
        {
            if(table->primaryKeys[i] == NULL)
                continue;
            free(table->primaryKeys[i]);
            table->primaryKeys[i] = NULL;
        }
        free(table->primaryKeys);
        free(table->indexPrimaryKeys);
        table->primaryKeys = NULL;
        table->numberOfPrimaryKeys = 0;
    }

    free(table);
    table = NULL;
}

void printTable(Table* table)
{
    if(table == NULL)
    {
        printf("Tabela Inexistente!\n");
        return;
    }

    if(table->name == NULL)
    {
        printf("Table name invalid\n");
    }
    else
        printf("\n\nNome: %s\n", table->name);

    if(table->attributes == NULL && table->numberOfAttributes != 0)
    {
        printf("\nTable Attributes invalid\n");
    }
    else
    {
        for(int i = 0; i < table->numberOfAttributes; i++)
        {
            if(table->attributes[i] == NULL)
            {
                printf("Table attribute %d not allocated\n", i);
                continue;
            }
            if(table->attributeType[i] == NULL)
            {
                printf("Table Attribute type %d not allocated\n", i);
                continue;
            }

            printf("Atributo %d: %s", i, table->attributes[i]);
            printf(" Tipo: %s \n",table->attributeType[i]);
        }
    }

    if(table->primaryKeys == NULL && table->numberOfPrimaryKeys != 0)
    {
        printf("\nTable Primary Keys invalid\n");
    }
    else
    {
        for(int i = 0; i < table->numberOfPrimaryKeys; i++)
        {
            if(table->primaryKeys[i] == NULL)
            {
                printf("Table Primary Key %d not allocated\n", i);
                continue;
            }

            printf("Primary Key %d: %s\n", i, table->primaryKeys[i]);
        }
        for(int i = 0; i < table->numberOfPrimaryKeys; i++)
            printf("Primary key index %d: %d\n", i, table->indexPrimaryKeys[i]);
    }

    if(table->foreignKeys == NULL && table->numberOfForeignKeys != 0)
    {
        printf("\nTable Foreign Keys invalid\n");
    }
    else
    {
        for(int i = 0; i < table->numberOfForeignKeys; i++)
        {
            if(table->foreignKeys[i] == NULL)
            {
                printf("Table Foreign Key %d not allocated\n", i);
                continue;
            }
            printf("Foreign Key %d: %s\n", i, table->foreignKeys[i]);
        }

        for(int i = 0; i < table->numberOfForeignKeys; i++)
            printf("Primary key index %d: %d\n", i, table->indexforeignKeys[i]);
    }

}

void printDataBase(DataBase* database)
{
    if(database == NULL)
    {
        printf("Banco inexistente!\n");
        return;
    }
    printf("\n\n=== Printing Data Base ===\n");
    for(int i = 0; i < database->numberOfTables; i++)
        printTable(database->tables[i]);

    printf("\n=== Print finished ===\n");
}

void freeDataBase(DataBase* database)
{
    if(database == NULL)
        return;

    if(database->tables != NULL)
    {
        for(int i = 0; i < database->numberOfTables; i++)
        {
            if(database->tables[i] == NULL)
                continue;
            freeTable(database->tables[i]);
            database->tables[i] = NULL;
        }
        free(database->tables);
        database->tables = NULL;
    }

    database->numberOfTables = 0;
    free(database);
    database = NULL;
}
