#include "rest.h"

// Temporariamente aqui
#define MAX_REST_HEADERS 5

RESTRequest* restRequest_init()
{
    RESTRequest *newRequest;
    newRequest = malloc(sizeof(RESTRequest));
    newRequest->header = malloc(sizeof(char*)*MAX_REST_HEADERS);
    for(int i = 0; i < MAX_REST_HEADERS; i++)
        newRequest->header[i] = NULL;
    newRequest->url = NULL;
    newRequest->numberOfHeaders = 0;
    newRequest->cursor = NULL;

    return newRequest;
}

// Retorna um Rest_Reques*t com informações identicas
RESTRequest* restRequest_copy(RESTRequest* request)
{
    if(request == NULL)
        return NULL;

    RESTRequest* newRequest = malloc(sizeof(RESTRequest));

    newRequest->numberOfHeaders = request->numberOfHeaders;
    newRequest->header = malloc(sizeof(char*)*newRequest->numberOfHeaders);
    int length = 0;
    for(int i = 0; i < request->numberOfHeaders; i++)
    {
        length = strlen(request->header[i]) + 1;
        newRequest->header[i] = malloc(sizeof(char)*length);
        newRequest->header[i] = strncpy(newRequest->header[i], request->header[i], length);
    }


    if(request->url == NULL)
    {
        newRequest->url = NULL;
    }
    else
    {
        length = strlen(request->url) + 1;
        newRequest->url = malloc(sizeof(char)*length);
        newRequest->url = strncpy(newRequest->url, request->url, length);
    }

    newRequest->cursor = NULL;

    return newRequest;

}

/// Define a URL da requisição rest
RESTRequest* restRequest_setUrl(RESTRequest *request, char* newURL)
{
    if(request == NULL)
        request = restRequest_init();

    if(request->url != NULL)
    {
        restRequest_freeUrl(request);
    }

    int stringSize = strlen(newURL);
    request->url = malloc(sizeof(char)*(stringSize+1));
    strncpy(request->url, newURL, stringSize);
    request->url[stringSize] = '\0';
    return request;
}

/// Define a URL da requisição rest
RESTRequest* restRequest_setCursor(RESTRequest *request, char* newCursor)
{
    if(request == NULL)
        request = restRequest_init();

    if(request->cursor != NULL)
    {
        restRequest_freeCursor(request);
    }

    if(newCursor == NULL)
        request->cursor = newCursor;
    else
    {
        int stringSize = strlen(newCursor);
        request->cursor = malloc(sizeof(char)*(stringSize+1));
        strncpy(request->cursor, newCursor, stringSize);
        request->cursor[stringSize] = '\0';
    }


    return request;
}

RESTRequest* restRequest_setHeader(RESTRequest* request, int index, char* toSet)
{
    if(request == NULL)
    {
        printf("\nRESTRequest does not exist.\n");
        return NULL;
    }

    if(index >= request->numberOfHeaders)
    {
        printf("Index not found, last index is %d.\n", request->numberOfHeaders-1);
        return NULL;
    }
    else if(index < 0)
    {
        printf("Invalid index, negative index not allowed.\n");
        return NULL;
    }

    if(request->header[index] != NULL)
        free(request->header[index]);


    int toSetSize = strlen(toSet);
    request->header[index] = malloc(sizeof(char)*(toSetSize + 1));
    strncpy(request->header[index], toSet, toSetSize);
    request->header[index][toSetSize] = '\0';

    return request;

}

/// Adiciona um header a requisição rest
RESTRequest* restRequest_appendHeader(RESTRequest *request, char* toAppend)
{
    if(request->numberOfHeaders == MAX_REST_HEADERS)
    {
        printf("Can not append, max number os headers reached\n");
        return NULL;
    }


    request->numberOfHeaders++;
    RESTRequest* newRequest = restRequest_setHeader(request, request->numberOfHeaders - 1, toAppend);

    if(newRequest == NULL)
    {
        printf("Failed to append header.\n");
        request->numberOfHeaders--;
        return NULL;
    }

    return newRequest;

}

/// Libera memória do RESTRequest
void restRequest_free(RESTRequest *request)
{
    restRequest_freeHeaders(request);
    restRequest_freeUrl(request);
    free(request);
}

/// Libera memória dos headers do RESTRequest
void restRequest_freeHeaders(RESTRequest *request)
{
    if(request->header == NULL)
        return;

    for(int i = 0; i < request->numberOfHeaders; i++)
    {
        if(request->header[i])
        {
            free(request->header[i]);
            request->header[i] = NULL;
        }
    }
    free(request->header);
    request->numberOfHeaders = 0;

}

/// Libera memória da url do RESTRequest
void restRequest_freeUrl(RESTRequest *request)
{
    if(request->url)
    {
        free(request->url);
        request->url = NULL;
    }
    return;
}

/// Libera memória da url do RESTRequest
void restRequest_freeCursor(RESTRequest *request)
{
    if(request->cursor)
    {
        free(request->cursor);
        request->cursor = NULL;
    }
    return;
}

