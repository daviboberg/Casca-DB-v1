#include "sql_query.h"

//#define PRINT_TABLES
//#define PRINT_RESULTS
//#define PRINT_EXECUTING

void query(DBConnection* connection, char* sql,char * buf)
{

#ifdef PRINT_EXECUTING
    printf("\n\nExecuting: %s \n", sql);
#endif // PRINT_EXECUTING

    int type = check_type(sql);
    Results* query_results;
    
    if(type == SQL_SELECT)
    {
        
        query_results = sql_select(connection, sql);

    }
    else if(type == SQL_CREATE_TABLE)
    {
        
        query_results = sql_createTable(connection, sql);
    }
    else if(type == SQL_INSERT)
    {
        
        query_results = sql_insert(connection, sql);
    }
    // Para remover do banco
    /*
    else if(type == SQL_DELETE)
    {
        sql_delete(connection, sql);
    }
    */
    
  // O resultado do select esta em select_result
    char* results ;
    
    
    if(query_results!=NULL){
      results = JSONFromResults(query_results);
      
      results_free(query_results);
      
      query_results = NULL;
  }

#ifdef PRINT_RESULTS
  printf(results);
#endif //PRINT_RESULTS_INSERT
  memcpy(buf,results,strlen(results));
    //printf("tambuf %d\n",strlen(buf) );
  
  if(query_results!=NULL)
  {
    free(results);    
}


db_disconnect( connection);


}

void nQueries(DBConnection* connection, char** sql, int n)
{
    for(int i = 0; i < n; i++)
    {
        //query(connection, sql[i]);
    }
}

