#include "request.h"

#include "../../API.h"



long process_RestReq(void*  socket,void* msg, long tam)
{

    info_socket_t* iS = (info_socket_t*) socket;
 
    char* string = msg;

    if(tam==-1)
    {
        iS->socket_data_input.lastCompleteMessage = calloc((iS->socket_data_input.buffer_size+2),sizeof(char));
        memcpy(iS->socket_data_input.lastCompleteMessage,iS->socket_data_input.buffer ,iS->socket_data_input.buffer_size+1);
        sem_post(iS->GPsem);
        while(iS->socket_data_input.lastCompleteMessage!=NULL)
        {
          usleep(10000);

        }
        return tam;
    }

    return 0;
}   








/// Consulta rest GET com o banco de dados
/// Retorno: CURLcode
/// Parametros:
///      request:
///          Tipo: RESTRequest*
///          Descrição: Armazena os parametros para realizar a comunicação com o banco
///                     (url, headers)
///      chunk:
///          Tipo: struct MemoryStruct*
///          Descrição: Armazena a mensagem de retorno do banco de dados
void GET(RESTRequest* connection, struct MemoryStruct* chunk)
{
    REST("GET", connection, NULL, chunk);
    return;

}

/// Consulta rest POST com o banco de dados
/// Retorno: void
/// Parametros:
///      request:
///          Tipo: RESTRequest*
///          Descrição: Armazena os parametros para realizar a comunicação com o banco
///                     (url, headers)
///      data:
///          Tipo: char*
///          Descrição: chave a ser adicionada ao banco de dados
///      chunk:
///          Tipo: struct MemoryStruct*
///          Descrição: Armazena a mensagem de retorno do banco de dados
void POST(RESTRequest* connection, char* data, struct MemoryStruct* chunk)
{
    REST("POST", connection, data, chunk);
}

void DELETE(RESTRequest* connection, struct MemoryStruct* chunk)
{
    REST("DELETE", connection, NULL, chunk);
    return;
}

char* REST(char* requestType, RESTRequest* connection, char* data, struct MemoryStruct* chunk)
{

    char stringToSend[10000];
    char page[1000];
    char ip2[100];
    int port;
    

    sscanf(connection->url, "http://%99[^:]:%99d/%99[^\n]", ip2, &port, page);

   
    char ip[1000];
    
   
   
    if(strlen(requestType)==3)
    {
    sprintf(stringToSend,"%s /%s HTTP/1.1\r\nAccept: */*\r\n%s\r\n%s\r\n\r\n",requestType,page,connection->header[0],connection->header[1]);
    }else
    {
    sprintf(stringToSend,"%s /%s HTTP/1.1\r\nAccept: */*\r\n%sContent-Type: application/json\r\nContent-Length:%d\r\n\r\n%s\r\n\r\n",requestType,page,connection->header[0],strlen(data),data);
    }

    
    info_socket_t* iS =API_create_socket("141.76.46.169", port,NULL, &(process_RestReq), NULL);
    
  
    while(!iS->isConnected){
       usleep(1);
      
    }
 

   API_write(iS,stringToSend,strlen(stringToSend),NULL); 

   sem_init(iS->GPsem, 1, 0);
   sem_wait(iS->GPsem);
  
 
   
    char * string = iS->socket_data_input.lastCompleteMessage;
    //printf("lastCompleteMessage:\n%s\ntam:\n%d\n", iS->socket_data_input.lastCompleteMessage,strlen(string));
    char* memtocpy = calloc(strlen(string),sizeof(char));


for (int i = 0; i <strlen(string); ++i)
{
  if(string[i]=='{')
  {
    memcpy(memtocpy,&(string[i]),strlen(string)-i);
    break;

  }
}
iS->socket_data_input.lastCompleteMessage=NULL;
free(iS->socket_data_input.lastCompleteMessage);


   
 

    chunk->memory = realloc(chunk->memory, chunk->size + strlen(memtocpy) + 1);
    memcpy(&(chunk->memory[chunk->size]),memtocpy,strlen(memtocpy));
    chunk->size +=strlen(memtocpy);
    chunk->memory[chunk->size] = 0;
    free(memtocpy);
    return 0;


    

}

size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (MemoryStruct *)userp;


    mem->memory = realloc(mem->memory, mem->size + realsize + 1);

    if(mem->memory == NULL) {
        //out of memory!
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}
