#include"Consulta_Sql.h"

int main(int argc, char** argv)
{

    char* sql = argv[1];//"SELECT Tabela1.Atributo1, Tabela2.Atributo2, Tabela3.Atributo3, Tabela4.Atributo4 FROM Tabela1, Tabela2, Tabela3 WHERE Tabela1.Atributo1 != Tabela2.Atributo2";
    consulta_select resultado_select;
    resultado_select = parse_sql_select(sql);
    print_consulta_select(resultado_select);

    char* insert = argv[2];//"INSERT INTO Cliente Values ('1755','Doriana','567.387.387-44')";
    consulta_insert resultado_insert;
    resultado_insert = parse_sql_insert(insert);
    print_consulta_insert(resultado_insert);
    return 0;

}
