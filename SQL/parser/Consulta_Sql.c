#include"Consulta_Sql.h"
#define MAX_MATCHES 10

char* str_between_delimiter(char* str, const char delim)
{
    int startDelimFound = 0;
    int strSize = strlen(str);
    int start = 0;
    int end = 0;
    int match = 0;

    int i = 0;
    while(i < strSize)
    {
        if(str[i] == delim )
        {
            if(startDelimFound == 0)
            {
                start = i;
                startDelimFound = 1;
            }
            else if(startDelimFound == 1)
            {
                end = i;
                match = 1;
                break;
            }
        }
        i++;
    }

    if(match == 0)
        return NULL;

    int length = end - start;
    char* newStr = malloc(sizeof(char)*length);
    strncpy(newStr, str + sizeof(char)*(start + 1), length);
    newStr[length-1] = '\0';
    return newStr;
}

List* str_split_between(char* a_str, const char a_delim)
{
    int startFound = 0;
    int startIndex = 0;
    int endIndex = 0;
    int notFound = 1;
    List* myList = NULL;
    int listSize = 0;
    char* string = NULL;

    for(int i = 0; a_str[i] != '\0'; i++)
    {
        if(a_str[i] == a_delim)
        {
            if(startFound == 0)
            {
                startFound = 1;
                startIndex = i + 1;
            }
            else
            {
                notFound = 0;
                endIndex = i - 1;
                startFound = 0;
                int stringSize = endIndex - startIndex + 1;
                string = calloc(stringSize + 1, sizeof(char));
                strncpy(string, a_str + startIndex, stringSize);
                myList = listAppend(myList, string);
                free(string);
            }
        }
    }
    if(notFound)
        return NULL;

    return myList;
}

//Funcao para separar os componentes de uma string dado um delimitador
List* str_split(char* a_str, const char a_delim)
{
    List* result    = NULL;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim_found = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    for(int i = 0; a_str[i] != '\0'; i++)
    {
        if(a_str[i] == a_delim)
        {
            count++;
            last_comma = i;
            delim_found = 1;
        }
    }

    if(delim_found == 1)
    {
        if((strlen(a_str) - 1) != last_comma)
            count++;
    }
    else
    {
        count++;
    }
    result = malloc(sizeof(List));
    result->values = malloc(sizeof(char*)*count);
    result->listSize = count;
    char* token = strtok(a_str, delim);
    int stringSize;
    for(int i = 0; i < count; i++)
    {
        stringSize = strlen(token);
        result->values[i] = malloc(sizeof(char)*(stringSize+1));
        strncpy(result->values[i], token, stringSize);
        result->values[i][stringSize] = '\0';
        token = strtok(NULL, delim);
    }

    return result;
}

//Funcao para retirar delimitadores e retornar a substring tratada
char* substring(char* string, int ini, int fim, char delimiter)
{
    if(ini < 0 || fim < 0)
        return NULL;
    int tam = fim - ini + 1;
    char *subs = calloc(tam, sizeof(char));

    int j = 0;
    for(int i = ini; i<fim; i++)
    {
        if(string[i]!=delimiter)
        {
            subs[j] = string[i];
            j++;
        }
    }
    //Caso o tamanho da string for menor do que o alocado
    if((j+1) < tam)
    {
        char *new_subs = realloc(subs, sizeof(char)*(j+1));
        if(new_subs == NULL)
        {
            printf("Problem at realloc\n");
            return subs;
        }
        return new_subs;
    }
    return subs;
}

//Processa o regex e retorna seu resultado
int get_match(regex_t* reg, char* sql, char** user_ptr, int group)
{

    regmatch_t matches[MAX_MATCHES];
    int regex__err = regexec(reg, sql, MAX_MATCHES, matches, group);
    if (regex__err == REG_NOERROR)
    {

        int inicio = matches[group].rm_so;
        int fim = matches[group].rm_eo;
        char* dest;
        dest = substring(sql,inicio,fim,'"');
        *user_ptr = dest;
        return REG_NOERROR;
    }
    else if(regex__err == REG_NOMATCH)
    {
        return REG_NOMATCH;
    }
    else
    {
        printf("\%s com erro de sintaxe\n", sql);

    }
    return -1;


}

void print_consulta_select(consulta_select c){

    printf("Print do Select: \n\n");
    int i = 0;
    printf("Atributos: \n");
    if(c.attributes)
    {
        for(int i = 0; i < c.numberOfAttributes; i++)
        {
            printf("[%s] ", c.attributes[i]);
        }
    }
    printf("\n\n");
    i = 0;
    printf("Tabelas: \n");
    if(c.tables)
    {
        for(int i = 0; i < c.numberOfTables; i++)
        {
            printf("[%s] ", c.tables[i]);
        }
    }
    printf("\n\n");
    i = 0;
    printf("Condicoes: \n");
    if(c.conditions)
    {
        for(int i = 0; i < c.numberOfConditions; i++)
        {
            printf("[%s] ", c.conditions[i]);
        }
    }
    printf("\n\n");

}

void print_consulta_insert(consulta_insert c){

    printf("Print do Insert: \n\n");
    int i = 0;
    printf("Tabela: \n");
    printf("[%s] ", c.table);
    printf("\n\n");

    printf("Valores: \n");
    for(int i = 0; i < c.numberOfValues; i++)
    {
        printf("[%s] ", c.values[i]);
    }
    printf("\n\n");

}

consulta_select* parse_sql_select(char* sql)
{

    consulta_select* resultado = calloc(1,sizeof(consulta_select));
    resultado->attributes = NULL;
    resultado->numberOfAttributes = 0;
    resultado->conditions = NULL;
    resultado->numberOfConditions = 0;
    resultado->tables = NULL;
    resultado->numberOfTables = 0;

    regex_t atributosPatern;
    regcomp(&atributosPatern,"\\s?(\\s)([a-zA-Z0-9\\-_\\.\\*]+)?((\\,\\s)([a-zA-Z0-9\\-_\\.\\*]+))*.", REG_EXTENDED);
    char* atributos = NULL;
    int matchAtributos = get_match(&atributosPatern, sql, &atributos, 0);
    if(matchAtributos == REG_NOERROR)
    {
        char* atributosTratados = substring(atributos,1,strlen(atributos),' ');
        List* temp = str_split(atributosTratados,',');
        resultado->attributes = temp->values;
        resultado->numberOfAttributes = temp->listSize;

        free(temp);
        free(atributosTratados);
    }
    regfree(&atributosPatern);
    free(atributos);


    char *sqlTratado;
    sqlTratado = strcasestr(sql,"FROM");
    if(sqlTratado == NULL)
    {
        printf("Invalid sql\n");
    }
    int position = sqlTratado - sql;
    char *novoSql = substring(sql,position,strlen(sql),'"');
    regex_t tabelasPatern;
    regcomp(&tabelasPatern,"(\\s[a-zA-Z0-9\\-_]+)((\\,\\s)([a-zA-Z0-9\\-_]+))*.", REG_EXTENDED);
    char* tabelas = NULL;
    int matchTabelas = get_match(&tabelasPatern, novoSql, &tabelas, 0);
    if(matchTabelas == REG_NOERROR)
    {
        char* tabelasTratadas = substring(tabelas,1,strlen(tabelas),' ');
        List* temp = str_split(tabelasTratadas,',');
        resultado->tables = temp->values;
        resultado->numberOfTables = temp->listSize;

        free(tabelasTratadas);
        free(temp);
    }
    regfree(&tabelasPatern);
    free(novoSql);
    free(tabelas);

    regex_t condicoesPatern;
    regcomp(&condicoesPatern,"([a-zA-Z0-9\\-_\\.]+)(\\s?\\s)(=|>|<|>=|<|<=|<>|!=)(\\s?\\s[a-zA-Z0-9\\-_\\.\\:]+)", REG_EXTENDED);
    char* condicoes = NULL;
    int matchCondicoes = get_match(&condicoesPatern, sql, &condicoes, 0);
    if(matchCondicoes == REG_NOERROR)
    {
        resultado->conditions = str_split(condicoes,' ');
        int i = 0;
        while(resultado->conditions[i] != NULL)
        {
            resultado->numberOfConditions++;
            i++;
        }

        regfree(&condicoesPatern);
    }


    return resultado;

}

consulta_insert* parse_sql_insert(char* sql)
{

    consulta_insert* resultado = calloc(1, sizeof(consulta_insert));
    resultado->table = NULL;
    resultado->values = NULL;
    resultado->numberOfValues = 0;

    regex_t tabelasPatern;
    regcomp(&tabelasPatern,"([i,I][n,N][t,T][o,O].*)\\s?(\\s)([a-zA-Z0-9\\-_\\.]+)?(\\s)", REG_EXTENDED);
    char* tabelas = NULL;
    int matchTabelas = get_match(&tabelasPatern, sql, &tabelas, 0);
    if(matchTabelas == REG_NOERROR)
    {
        List* temp = str_split(tabelas,' ');
        if(temp->listSize >= 1)
        {
            int stringSize = strlen(temp->values[1])+ 1;
            resultado->table = malloc(sizeof(char)*stringSize);
            strncpy(resultado->table, temp->values[1], stringSize);
        }
        for(int i = 0; i < temp->listSize; i++)
        {
            free(temp->values[i]);
        }
        free(temp->values);
        free(temp);
    }
    regfree(&tabelasPatern);
    free(tabelas);

    regex_t valoresPatern;
    regcomp(&valoresPatern,"(')(.*?)(')", REG_EXTENDED);
    char* valores = NULL;
    int matchValores = get_match(&valoresPatern, sql, &valores, 0);
    if(matchValores == REG_NOERROR)
    {
        char* valoresTratados = substring(valores, 0, strlen(valores), ' ');
        List* tmp = str_split_between(valoresTratados,'\'');

        if(tmp != NULL)
        {
            resultado->numberOfValues = tmp->listSize;
            resultado->values = tmp->values;
            free(tmp);
        }
        free(valoresTratados);
    }
    regfree(&valoresPatern);
    free(valores);

    return resultado;

}

Table* parse_sql_create_table(char* sql)
{

    Table* table;
    table = calloc(1,sizeof(Table));

    // Nome da tabela
    regex_t tabelasPatern;
    regcomp(&tabelasPatern,"[t,T][a,A][b,B][l,L][e,E]((\\s+)[a-zA-Z0-9\\.\\-\\_\\:]+)", REG_EXTENDED);
    char* tabela = NULL;
    int matchTabela = get_match(&tabelasPatern, sql, &tabela, 0);
    if(matchTabela == REG_NOERROR)
    {

        List* temp = str_split(tabela,' ');
        int stringSize = strlen(temp->values[1])+ 1;
        table->name = malloc(sizeof(char)*stringSize);
        strncpy(table->name, temp->values[1], stringSize);
        for(int i = 0; i < temp->listSize; i++)
        {
            free(temp->values[i]);
        }
        free(temp->values);
        free(temp);
    }
    regfree(&tabelasPatern);
    free(tabela);

     // Foreign Key
    regex_t foreignPatern;
    regcomp(&foreignPatern,"[f,F][o,O][r,R][e,E][i,I][g,G][n,N]\\s[k,K][e,E][y,Y]\\s*?\\((.+?)\\)", REG_EXTENDED);
    char* foreignKeys = NULL;
    int matchForeignKeys = get_match(&foreignPatern, sql, &foreignKeys, 1);
    if(matchForeignKeys == REG_NOERROR)
    {
        char* semParenteses = substring(foreignKeys,0, strlen(foreignKeys),')');


        char* keysTratadas = substring(semParenteses,0,strlen(semParenteses),' ');
        List* temp = str_split(keysTratadas,',');
        table->foreignKeys = temp->values;
        table->numberOfForeignKeys = temp->listSize;

        free(temp);
        free(keysTratadas);
        free(semParenteses);
    }
    regfree(&foreignPatern);
    free(foreignKeys);


    // Primary Key
    char *sqlFim, *sqlInicio;
    sqlFim = strcasestr(sql,"FOREIGN KEY");
    sqlInicio = strcasestr(sql,"PRIMARY KEY");
    int positionInicio = sqlInicio - sql;
    int positionFim = 0;
    if(sqlFim != NULL)
        positionFim = sqlFim - sql;
    else
        positionFim = strlen(sql);
    char *novoSql = substring(sql,positionInicio,positionFim,'"');
    regex_t keyPatern;
    regcomp(&keyPatern,"[p,P][r,R][i,I][m,M][a,A][r,R][y,Y]\\s[k,K][e,E][y,Y]\\s*?\\((.+?)\\)", REG_EXTENDED);
    char* keys = NULL;
    int matchKeys = get_match(&keyPatern, novoSql, &keys, 1);
    if(matchKeys == REG_NOERROR)
    {
        char* semParenteses = substring(keys,0, strlen(keys),')');
        char* keysTratadas = substring(semParenteses,0,strlen(keys),' ');
        List* temp = str_split(keysTratadas,',');
        table->primaryKeys = temp->values;
        table->numberOfPrimaryKeys = temp->listSize;

        free(temp);
        free(keysTratadas);
        free(semParenteses);
    }
    regfree(&keyPatern);
    free(novoSql);

    free(keys);


    // Atributos
    sqlFim = strcasestr(sql,"PRIMARY KEY");
    positionFim = sqlFim - sql;
    novoSql = substring(sql,0,positionFim-1,'"');
    regex_t attrPatern;
    regcomp(&attrPatern,"\\(.*,?", REG_EXTENDED);
    char* attributes = NULL;
    int matchAttributes = get_match(&attrPatern, novoSql, &attributes, 0);
    if(matchAttributes == REG_NOERROR)
    {
        char* atributosTratados = substring(attributes,1,strlen(attributes),'\'');
        List* atributos = str_split(atributosTratados,',');

        regex_t valuesPatern;
        regcomp(&valuesPatern,"([a-zA-Z0-9\\*_\\.\\-]+)(\\s*)([a-zA-Z0-9\\*\\.\\-\\(\\)]+)?", REG_EXTENDED);

        table->numberOfAttributes = atributos->listSize;
        table->attributes = calloc(table->numberOfAttributes,sizeof(char*));
        table->attributeType = calloc(table->numberOfAttributes, sizeof(char*));


        table->numberOfAttributes = 0;
        for(int i = 0; i < atributos->listSize; i++)
        {
            table->numberOfAttributes++;
            char* atributo = NULL;
            char* tipo = NULL;
            int matchAtributo = get_match(&valuesPatern, atributos->values[i], &atributo,1);
            if(matchAtributo == REG_NOERROR)
            {
                table->attributes[i] = atributo;
            }

            int matchTipo = get_match(&valuesPatern, atributos->values[i], &tipo,3);
            if(matchTipo == REG_NOERROR && tipo != NULL)
            {
                table->attributeType[i] = tipo;
            }
            else if((matchTipo == REG_NOERROR) && (tipo == NULL))
            {
                int stringSize;
                // Usa o tipo padrão
                char* defaultType = "VARCHAR(30)";
                stringSize = strlen(defaultType) + 1;
                table->attributeType[i] = malloc(sizeof(char)*stringSize);
                strncpy(table->attributeType[i], defaultType, stringSize);
            }
        }
        free(atributosTratados);
        for(int i = 0; i < table->numberOfAttributes; i++)
        {
            free(atributos->values[i]);
        }
        free(atributos->values);
        free(atributos);
        regfree(&valuesPatern);
    }

    if(table->numberOfPrimaryKeys > 0)
        table->indexPrimaryKeys = malloc(sizeof(int)*table->numberOfPrimaryKeys);
    int indexPK = 0;
    int indexFK = 0;
    int hasNew = 0;
    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        hasNew = 0;
        if(indexPK < table->numberOfPrimaryKeys)
        {
            hasNew = 1;
            for(int j = 0; j < table->numberOfPrimaryKeys; j++)
            {
                if(strcmp(table->attributes[i], table->primaryKeys[j]) == 0)
                {
                    table->indexPrimaryKeys[indexPK] = i;
                    indexPK++;
                }
            }
        }

        if(indexFK < table->numberOfForeignKeys)
        {
            hasNew = 1;
            for(int j = 0; j < table->numberOfForeignKeys; j++)
            {
                if(strcmp(table->attributes[i], table->foreignKeys[j]) == 0)
                {
                    table->indexforeignKeys[indexFK] = i;
                    indexFK++;
                }
            }
        }

        if(!hasNew)
            break;
    }

    regfree(&attrPatern);
    free(novoSql);
    free(attributes);


    return table;

}

int check_type(char* sql)
{
    regex_t sqlPattern;
    regcomp(&sqlPattern,"(\\s*)?([a-zA-Z0-9]+)?", REG_EXTENDED);

    char* word = NULL;
    int type;
    int matchOperation = get_match(&sqlPattern, sql, &word, 0);
    if(matchOperation == REG_NOERROR)
    {
        char* operation = substring(word,0,strlen(word),' ');

        if(strcasecmp(operation, "SELECT") == 0)
        {
            printf("Select");
            type = SQL_SELECT;
        }
        else if(strcasecmp(operation, "CREATE") == 0)
        {
            char* secondWord = NULL;
            int matchOperation2 = get_match(&sqlPattern, sql + strlen(word), &secondWord, 0);
            if(matchOperation2 == REG_NOERROR)
            {
                char* operation2 = substring(secondWord,0, strlen(secondWord), ' ');
                if(strcasecmp(operation2, "TABLE") == 0)
                    type = SQL_CREATE_TABLE;
            }

        }
        else if(strcasecmp(operation, "INSERT") == 0)
        {
            type = SQL_INSERT;
        }
        else if(strcasecmp(operation, "DELETE") == 0)
        {
            type = SQL_DELETE;
        }
        else
            type = SQL_NO_MATCH;
        free(operation);
    }
    regfree(&sqlPattern);
    free(word);

    return type;
}
