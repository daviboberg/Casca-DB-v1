#include "threads.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data_structs.h"

void cmd_dummy (info_cmd_t* current_task);
void cmd_read (info_cmd_t* current_task);
void cmd_accept (info_cmd_t* current_task);
void cmd_write(info_cmd_t* current_task);
void cmd_connect(info_cmd_t* current_task);
#ifdef TLS
void cmd_accept_tls(info_cmd_t* current_task);
void cmd_read_tls(info_cmd_t* current_task);
void cmd_write_tls(info_cmd_t* current_task);
void cmd_connect_tls(info_cmd_t* current_task);
#endif


