/**
 * @file data_structs.h
 * @author Pedro, Lucas and Marcelo
 * @date 14 Aug 2017
 * @brief Contains all data structs of Shell.
 * 
 */
#ifndef __DATA_STRUCTS__
#define __DATA_STRUCTS__

#include <stdbool.h>
#include "define.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <semaphore.h>

/** @brief Enum to define the type of a info_socket_t.*/
typedef enum socket_type_enum {SERVER /**< Only recieve connections and creates new IO sockets for then. */
							   ,IO    /**< Can only send and recieve messages.*/
							   ,SQL_SERVER
							   ,SQL_IO 
							   ,TLS_SERVER
							   ,TLS_IO    
							  }socket_type_t;

/** @brief Stores the file descriptor of a socket.*/
typedef int socket_t;

/** @brief Callback which is triggered when a message is recieved.
 *  @param buffer Buffer that recieves the message.
 *  @param buffer_size Size in bytes of the message in \b buffer.
 */
typedef long (* process_func_t) (void* isocket,void * buffer, long buffer_size);

/** @brief Callback which is triggered when a message is sent.*/
typedef void (* send_done_func_t)();

/** @brief Generic callback that is used to implement minor features.*/
typedef void  (*task_func)();


/** @brief Struct to store data that is recieved by socket associated to it. (W.I.P.)
 *
 * \todo Implement a memory buffer to re-send the same message adding the new parts of it until the process function says that the message is complete
*/
typedef struct {
	/* buffer de dados, cujo tamanho máximo é buffer_size.
	   buffer_used indica quantos bytes foram lidos até agora */
	void * buffer;
	long   buffer_size;
	void * lastCompleteMessage;
	//long   buffer_used;

	/* callback chamada cada vez que novos dados são lidos.
	   Caso retorne valor não-nulo, significa que a mensagem completa chegou
	   e o valor retornado é o tamanho da mensagem completa. O buffer deve
	   ser "zerado", isto é, eliminar tudo entre [0, <retorno da funcao>
	   e buffer_used deve ser ajustado. */
	process_func_t  process;/**<@brief Pointer to callback which is triggered when a message is recieved. */
} input_socket_data;






typedef struct {
	struct info_cmd_queue * prev;
	struct info_cmd_queue * next;	
	void * buffer;
	long   buffer_size;
	
	long free_index;
	
	send_done_func_t  send_done;
} message_queue_t;




/** @brief Struct to store data that is in queue to be sent by the socket associated to it. (W.I.P.)
 *
 * \todo Implement a queue of messaged with buffers instead of a single big buffer.
*/
typedef struct {
	message_queue_t* message_queue;
	send_done_func_t  send_done;
} output_socket_data;


/** @brief Struct to store data related to a specific socket*/
typedef struct {
	socket_t        socket; 			/**<@brief Int (typedef) to store socket file descriptor. */
	bool            select_ready;		/**<@brief Boolean to tell if the socket is ready to be scanned by select threads. */
	bool 			isConnected;		/**<@brief Boolean that is set to true once the socket is connected.
										 * @warning This boolean is used only internally by the shell and shoulnd't be used as a real-time indicator outside of it.*/
	socket_type_t   type;				/**<@brief Enum that can be SERVER or IO. */
	bool inRead;						/**<@brief Boolean to avoid two threads reading a socket at same time. */
	bool inWrite;						/**<@brief Boolean to avoid two threads writing in a socket at same time. */
	struct sockaddr_in info;			/**<@brief Struct used by socket.h to specify an address. */
	int id;								/**<@brief Simple numerical identifier for DEBUG purposes. */
	bool shutdown;						/**<@brief Boolean that is set when the socket is on process of shutdown. */

	sem_t *GPsem;

	pthread_mutex_t *GPmutex;

	input_socket_data socket_data_input;/**<@brief Struct to store data that is in queue to be sent by the socket.*/
	output_socket_data socket_data_output;/**<@brief Struct to store data that is recieved by the socket.  */
	SSL_CTX* socket_context;			/**<@brief Struct to store ctx data.  */
	SSL* socket_tls;					/**<@brief TLS socket file descriptor.  */
} info_socket_t;


//typedef enum cmd_enum {SOCKET_READ, SOCKET_WRITE} cmd_t;

/** @brief Struct to store data related to a command in the task queue*/
typedef struct {
	task_func cmd;						/**<@brief Pointer to the function wich will be executed by the thread that removed this command from the queue. */
	info_socket_t * socket;				/**<@brief Pointer to a info_socket_t that this command refer to */
	task_func connected;				/**<@brief Pointer to a function that is called when the socket is sucessfully connected for the first time */
	void *  data_slot;					/**<@brief Pointer to data that is used by cmd */
} info_cmd_t;

/** @brief Struct to implemet the task queue*/
typedef struct info_cmd_queue {
	struct info_cmd_queue * prev;		/**<@brief Pointer to previous in queue. */
	struct info_cmd_queue * next;		/**<@brief Pointer to next in queue. */
	info_cmd_t       cmd;				/**<@brief Command to be executed and its data. */
	int id;								/**<@brief Simple integer identifier. (not workin properly)*/
} info_cmd_queue_t;

/** @brief (W.I.P.)*/
typedef struct config_struct
{
	int max_threads;
	int max_sqlthreads;	
	int sql_port;
	char* db_url;
	char* db_key;			

} config_struct_t;

/** @brief Data that is used in data_slot of info_cmd_t when the command is to accept some conection*/
typedef struct {

	info_socket_t* IOvec;				/**<@brief Vector of IO info_socket_t that is scanned by select threads. */
	
	pthread_mutex_t* IOvec_mutex;		/**<@brief Mutex to access IO sockets vectors. */

} accept_socket_data;






typedef struct {
struct sql_req * prev;		/**<@brief Pointer to previous in queue. */
struct sql_req * next;		/**<@brief Pointer to next in queue. */
info_socket_t* outputsocket;
char* sql_statement;
long sql_size;
// introduzir sock choc e semaforos
} sql_req;




#endif
